.data
.L0:
    .quad 13
    .ascii "hello world!\n"
.text
.global _start
_start:
    movq %rsp, %rbp
    subq $0, %rsp
    movq $.L0, %rax
    movq 0(%rax), %rdx
    leaq 8(%rax), %rsi
    movq $1, %rax
    xorq %rdi, %rdi
    syscall
    movq $60, %rax
    movq $0, %rdi
    syscall

.data
    .quad 1
    .quad 0
.text
.L0:
    pushq %rbp
    movq %rsp, %rbp
    subq $0, %rsp
    movq %rdi, %rcx
    leaq .L4, %rbx
    cmpq $1, %rcx
    je .L2
    leaq .L5, %r8
    cmpq $0, %rcx
    je .L3
    leaq .L6, %rax
    movq %rax, 
    movq %rcx, %rdi
    callq *
    movq %rax, 
    movq , %r9
    jmp .L1
.L2:
    movq %rcx, %rdi
    callq *%rbx
    movq %rax, 
    movq , %r9
    jmp .L1
.L3:
    movq %rcx, %rdi
    callq *%r8
    movq %rax, 
    movq , %r9
    jmp .L1
.L1:
    movq %r9, %rax
    movq %rbp, %rsp
    popq %rbp
    ret
.L4:
    pushq %rbp
    movq %rsp, %rbp
    subq $0, %rsp
    movq $1, %rax
    movq %rbp, %rsp
    popq %rbp
    ret
.L5:
    pushq %rbp
    movq %rsp, %rbp
    subq $0, %rsp
    movq $0, %rax
    movq %rbp, %rsp
    popq %rbp
    ret
.L6:
    pushq %rbp
    movq %rsp, %rbp
    subq $0, %rsp
    movq %rdi, 
    movq , %rax
    movq %rbp, %rsp
    popq %rbp
    ret
.global _start
_start:
    movq %rsp, %rbp
    subq $0, %rsp
    leaq .L0, %rax
    movq %rax, 
    movq , %rax
    movq %rax, 
    movq $60, %rax
    movq $0, %rdi
    syscall

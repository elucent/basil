#ifndef BASIL_TERM_H
#define BASIL_TERM_H

#include "defs.h"
#include "vec.h"
#include "utf8.h"
#include "value.h"

namespace basil {
    class TermClass {
        const TermClass* _parent;
    public:
        TermClass();
        TermClass(const TermClass& parent);
        const TermClass* parent() const;
    };

    class Term {
        u32 _line, _column;
        Term* _parent;
        const TermClass* _termclass;
    protected:
        void indent(stream& io, u32 level) const;
    public:
        static const TermClass CLASS;

        Term(u32 line, u32 column, const TermClass* tc = &CLASS);
        virtual ~Term();
        u32 line() const;
        u32 column() const;
        void setParent(Term* parent);
        virtual void format(stream& io, u32 level = 0) const = 0;
        virtual void eval(Stack& stack) = 0;
        virtual bool equals(const Term* other) const = 0;
        virtual u64 hash() const = 0;

        template<typename T>
        bool is() const {
            const TermClass* ptr = _termclass;
            while (ptr) {
                if (ptr == &T::CLASS) return true;
                ptr = ptr->parent();
            }
            return false;
        }

        template<typename T>
        T* as() {
            return (T*)this;
        }

        template<typename T>
        const T* as() const {
            return (const T*)this;
        }
    };

    class IntegerTerm : public Term {
        i64 _value;
    public:
        static const TermClass CLASS;

        IntegerTerm(i64 value, u32 line, u32 column, 
                    const TermClass* tc = &CLASS);
        i64 value() const;
        virtual void format(stream& io, u32 level = 0) const override;
        virtual void eval(Stack& stack) override;
        virtual bool equals(const Term* other) const override;
        virtual u64 hash() const override;
    };

    class RationalTerm : public Term {
        double _value;
    public:
        static const TermClass CLASS;

        RationalTerm(double value, u32 line, u32 column,
                     const TermClass* tc = &CLASS);
        double value() const;
        virtual void format(stream& io, u32 level = 0) const override;
        virtual void eval(Stack& stack) override;
        virtual bool equals(const Term* other) const override;
        virtual u64 hash() const override;
    };

    class StringTerm : public Term {
        ustring _value;
    public:
        static const TermClass CLASS;

        StringTerm(const ustring& value, u32 line, u32 column,
                   const TermClass* tc = &CLASS);
        const ustring& value() const;
        virtual void format(stream& io, u32 level = 0) const override;
        virtual void eval(Stack& stack) override;
        virtual bool equals(const Term* other) const override;
        virtual u64 hash() const override;
    };

    class CharTerm : public Term {
        uchar _value;
    public:
        static const TermClass CLASS;

        CharTerm(uchar value, u32 line, u32 column,
                 const TermClass* tc = &CLASS);
        uchar value() const;
        virtual void format(stream& io, u32 level = 0) const override;
        virtual void eval(Stack& stack) override;
        virtual bool equals(const Term* other) const override;
        virtual u64 hash() const override;
    };

    class VariableTerm : public Term {
        ustring _name;
    public:
        static const TermClass CLASS;

        VariableTerm(const ustring& name, u32 line, u32 column,
                     const TermClass* tc = &CLASS);
        const ustring& name() const;
        virtual void format(stream& io, u32 level = 0) const override;
        virtual void eval(Stack& stack) override;
        virtual bool equals(const Term* other) const override;
        virtual u64 hash() const override;
    };

    class BlockTerm : public Term {
        vector<Term*> _children;
    public:
        static const TermClass CLASS;

        BlockTerm(const vector<Term*>& children, u32 line, u32 column,
                  const TermClass* tc = &CLASS);
        ~BlockTerm();
        const vector<Term*>& children() const;
        void add(Term* child);
        virtual void format(stream& io, u32 level = 0) const override;
        virtual void eval(Stack& stack) override;
        virtual bool equals(const Term* other) const override;
        virtual u64 hash() const override;
    };

    class LambdaTerm : public Term {
        Term *_arg, *_ret;
    public:
        static const TermClass CLASS;

        LambdaTerm(Term* arg, Term* ret, u32 line, u32 column,
                   const TermClass* tc = &CLASS);
        ~LambdaTerm();
        const Term* arg() const;
        const Term* ret() const;
        Term* arg();
        Term* ret();
        virtual void format(stream& io, u32 level = 0) const override;
        virtual void eval(Stack& stack) override;
        virtual bool equals(const Term* other) const override;
        virtual u64 hash() const override;
    };

    class AssignTerm : public Term {
        Term *_dst, *_src;
    public:
        static const TermClass CLASS;

        AssignTerm(Term* dst, Term* src, u32 line, u32 column,
                   const TermClass* tc = &CLASS);
        ~AssignTerm();
        const Term* dst() const;
        const Term* src() const;
        Term* dst();
        Term* src();
        virtual void format(stream& io, u32 level = 0) const override;
        virtual void eval(Stack& stack) override;
        virtual bool equals(const Term* other) const override;
        virtual u64 hash() const override;
    };

    class ProgramTerm : public Term {
        vector<Term*> _children;
        Stack *root, *global;

        void initRoot();
    public:
        static const TermClass CLASS;

        ProgramTerm(const vector<Term*>& children, u32 line, u32 column,
                    const TermClass* tc = &CLASS);
        ~ProgramTerm();
        const vector<Term*>& children() const;
        void add(Term* child);  
        virtual void format(stream& io, u32 level = 0) const override;  
        virtual void eval(Stack& stack) override;
        void evalChild(Stack& stack, Term* t);
        Stack& scope();
        virtual bool equals(const Term* other) const override;
        virtual u64 hash() const override;
    };
}

#endif
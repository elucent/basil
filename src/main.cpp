#include "vec.h"
#include "utf8.h"
#include "hash.h"
#include "stdio.h"
#include "stdlib.h"
#include "source.h"
#include "time.h"
#include "lex.h"
#include "parse.h"
#include "type.h"
#include "term.h"
#include "value.h"
#include "ir.h"

using namespace basil;

bool interactive = true;
bool printasm = true;
bool printtac = false;
Source* src = nullptr;
ustring outfile;
ustring infile;

ustring stripEnding(const ustring& path) {
    ustring n;
    i64 lastDot = -1;
    for (u32 i = 0; i < path.size(); i ++) {
        if (path[i] == '.') lastDot = i;
    }
    for (u32 i = 0; i < path.size(); i ++) {
        if (i < lastDot || lastDot <= 0) n += path[i];
    }
    return n;
}

void printCode(CodeGenerator& gen) {
    if (!printasm) {
        buffer b;
        fprint(b, outfile);
        string outpath;
        while (b.peek()) outpath += b.read();

        FILE* out = fopen((const char*)outpath.raw(), "w+");  
        file outfile(out);
        
        buffer text, data;
        if (printtac) gen.format(text);
        else gen.emitX86(text, data);
        fprint(outfile, data, text);
    }
    else {
        println("");
        // gen.format(_stdout);
        buffer text, data;
        if (printtac) gen.format(text);
        else gen.emitX86(text, data);
        fprint(_stdout, data, text);
        println("");
    }
}

void repl() {
    CodeGenerator gen;
    useSource(src);
    TokenCache cache(src);
    ProgramTerm* program = new ProgramTerm({}, 1, 1);
    while (!countErrors()) {
        print("? ");
        TokenCache::View view = cache.expand(_stdin);
        if (!countErrors()) {
            // println("");
            // println(cache);
            // println("");
        }
        else {
            printErrors(_stdout);
            continue;
        }
        
        if (view.peek().type == TOKEN_IDENT && view.peek().value == "quit") {
            println("Leaving REPL...");
            break;
        }

        Term* t = parse(view, true);
        if (!countErrors()) {
            // println("Parse output:");
            // t->format(_stdout);
            // println("");
        }
        else {
            printErrors(_stdout);
            continue;
        }
        Stack* s = new Stack();
        program->add(t);
        program->evalChild(*s, t);
        if (!countErrors()) {
            // println("");
            // for (Value* v : *s) v->format(_stdout);
            // println("");
        }
        else {
            printErrors(_stdout);
            continue;
        }

        for (Value* v : *s) v->gen(program->scope(), gen, gen);
        gen.allocate();
        // gen.format(_stdout);

        delete s;
    }
    
    if (!countErrors()) printCode(gen);

    delete program;
    delete src;
}

int parseArgs(int argc, char** argv) {
    while (argc) {
        if (string(*argv) == "-c") {
            if (argc == 1) {
                println("Error: '-c' was provided without an argument.");
                return 1;
            }
            buffer b;
            fprint(b, string(*(--argc, ++argv)));
            src = new Source(b);
        }
        else if (string(*argv) == "-o") {
            if (argc == 1) {
                println("Error: '-o' was provided without an argument.");
                return 1;
            }
            printasm = false;
            outfile = ustring(*(--argc, ++argv));
        }
        else if (string(*argv) == "-tac") {
            printtac = true;
        }
        else {
            infile = ustring(*argv);
            interactive = false;
            src = new Source(*argv);
        }
        --argc, ++ argv;
    }
    if (!src) {
        if (interactive) src = new Source();
        else {
            println("Error: no source or '-i' provided.");
            return 1;
        }
    }
    return 0;
}

int main(int argc, char** argv) {
    if (auto code = parseArgs(argc - 1, argv + 1)) return code;
    if (outfile.size() == 0 && !printasm) {
        if (infile.size() > 0) {
            outfile = stripEnding(infile) + ".s";
        }
        else outfile = "output.s";
    }

    if (interactive) {
        repl();
        return countErrors();
    }

    useSource(src);
    TokenCache cache(src);
    ProgramTerm* program = new ProgramTerm({}, 1, 1);
    cache = lex(*src);
    if (countErrors()) {
        printErrors(_stdout);
        return 1;
    }

    TokenCache::View v = cache.view();
    program = parseFull(v);
    if (countErrors()) {
        printErrors(_stdout);
        return 1;
    }
    
    Stack s;
    program->eval(s);
    if (countErrors()) {
        printErrors(_stdout);
        return 1;
    }
    
    // for (Value* v : s) v->format(_stdout);

    CodeGenerator gen;
    for (Value* v : s) v->gen(program->scope(), gen, gen);
    gen.allocate();
    // gen.format(_stdout);
    
    if (!countErrors()) printCode(gen);
    
    delete src;
    return countErrors();
}

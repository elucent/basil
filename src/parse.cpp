#include "parse.h"
#include "vec.h"
#include "term.h"
#include "io.h"

namespace basil {
    static bool repl_mode;

    // todo: clean this mess up :(

    template<typename... Args>
    static void err(TokenCache::View& t, Args... message) {
        if (t.cache().source()) {
            err(PHASE_PARSE, t.cache().source(), t.peek().line, 
                t.peek().column, message...);
        }
        else {
            err(PHASE_PARSE, t.peek().line, t.peek().column, message...);
        }
    }

    Term* parseLine(TokenCache::View& view, u32 indent, bool consume = true);

    Term* parseIndented(TokenCache::View& view, u32 indent, u32 prev) {
        vector<Term*> terms;
        const Token first = view.peek();

        if (!view.peek() && repl_mode) {
            print(". ");
            view.cache().expand(_stdin);
        }

        while (view.peek() && view.peek().column > prev) {
            terms.push(parseLine(view, indent));

            if (!view.peek() && repl_mode) {
                print(". ");
                view.cache().expand(_stdin);
            }
        }
        return new BlockTerm(terms, first.line, first.column);
    }
    
    Term* parseEnclosed(TokenCache::View& view, u32 end, u32 indent) {
        vector<Term*> terms;
        bool undoNesting = true;
        const Token first = view.peek();
        view.read(); // consume opening token
        while(view.peek().type != end) {
            if (view.peek().type == TOKEN_NEWLINE) {
                view.read(); // ignore empty lines
                continue;
            }
            if (view.peek().type == TOKEN_LPAREN
                || view.peek().type == TOKEN_LBRACE) {
                undoNesting = false; // maintain structure of nested parens
            }
            terms.push(parseLine(view, indent));

            if (!view.peek()) {
                if (repl_mode) {
                    print(_stdout, ". ");
                    view.cache().expand(_stdin);
                }
                else {
                    err(view, "Unexpected end of input.");
                    break;
                }
            }
        }
        view.read(); // consume ending token
	
	    return undoNesting && terms.size() == 1 ?
	        terms[0] : new BlockTerm(terms, first.line, first.column);
    }

    Term* parsePrimary(TokenCache::View& view, u32 indent) {
        buffer b;
        const Token t = view.peek();
        if (t.type == TOKEN_NUMBER) {
            view.read();
            fprint(b, t.value);
            for (u32 i = 0; i < t.value.size(); ++ i) {
                if (t.value[i] == '.') {
                    double v;
                    fread(b, v);
                    return new RationalTerm(v, t.line, t.column);    
                }
            }
            i64 v;
            fread(b, v);
            return new IntegerTerm(v, t.line, t.column);
        }
        else if (t.type == TOKEN_STRING) {
            view.read();
            return new StringTerm(t.value, t.line, t.column);
        }
        else if (t.type == TOKEN_CHAR) {
            view.read();
            return new CharTerm(t.value[0], t.line, t.column);
        }
        else if (t.type == TOKEN_IDENT) {
            view.read();
            return new VariableTerm(t.value, t.line, t.column);
        }
        else if (t.type == TOKEN_LPAREN) {
            return parseEnclosed(view, TOKEN_RPAREN, indent);
        }
        else if (t.type == TOKEN_LBRACE) {
            return parseEnclosed(view, TOKEN_RBRACE, indent);
        }
        else if (t.type == TOKEN_NEWLINE) {
            view.read();
            return nullptr;
        }
        else {
            view.read();
            err(view, "Unexpected token '", t.value, "'.");
            return nullptr;
        }
    }

    Term* parseLine(TokenCache::View& view, u32 indent, bool consume) {
        vector<Term*> nested;
        vector<Term*> terms;
        const Token begin = view.peek();
        while (view.peek()) {
            const Token t = view.peek();
            if (t.type == TOKEN_NEWLINE 
                || t.type == TOKEN_RPAREN
                || t.type == TOKEN_RBRACE) break;
            if (t.type == TOKEN_COLON) {
                view.read();
                Token s = view.peek();
                if (s.type == TOKEN_NEWLINE) {
                    view.read();
                    const Token& u = view.peek();
                    terms.push(parseIndented(view, u.column, indent));
                }
                else terms.push(parseLine(view, indent, false));
            }
            else if (t.type == TOKEN_SEMI) {
                view.read();
                BlockTerm* b = new BlockTerm(terms, t.line, t.column);
                nested.push(b);
                terms.clear();
            }
            else if (t.type == TOKEN_LAMBDA) {
                view.read();
                Term* l = parseLine(view, indent, false);
                if (terms.size() == 0) {
                    BlockTerm* b = new BlockTerm({}, t.line, t.column);
                    l = new LambdaTerm(b, l, t.line, t.column);
                }
                else {
                    BlockTerm* b = new BlockTerm(terms, t.line, t.column);
                    l = new LambdaTerm(b, l, t.line, t.column);
                }
                return l;
            }
            else if (t.type == TOKEN_ASSIGN) {
                view.read();
                Term* l = parseLine(view, indent, false);
                if (terms.size() == 0) {
                    err(PHASE_PARSE, view.peek().line, view.peek().column,
                        "No values on left side of assignment.");
                    return nullptr;
                }
                l = new AssignTerm(new BlockTerm(terms, t.line, t.column), 
                                   l, t.line, t.column);
                terms.clear();
                terms.push(l);
            }
            else if (t.type == TOKEN_DOT) {
                view.read();
                if (terms.size() > 0) {
                    Term* b = terms.back();
                    terms.pop();
                    Term* n = parsePrimary(view, indent);
                    if (n) terms.push(new BlockTerm({ b, n }, 
                        b->line(), b->column()));
                    else {
                        err(PHASE_PARSE, t.line, t.column,
                            "Expected term following '.' token.");
                    }
                }
                else if (nested.size() > 0) {
                    Term* b = nested.back();
                    nested.pop();
                    Term* n = parsePrimary(view, indent);
                    if (n) nested.push(new BlockTerm({ b, n }, 
                        b->line(), b->column()));
                    else {
                        err(PHASE_PARSE, t.line, t.column,
                            "Expected term following '.' token.");
                    }
                }
                else {
                    err(PHASE_PARSE, t.line, t.column,
                        "Expected term preceding '.' token.");
                }
            }
            else {
                Term* t = parsePrimary(view, indent);
                if (t) terms.push(t);
            }
        }

        if (view.peek().type == TOKEN_NEWLINE && consume) view.read();
        
        if (nested.size() > 0) {
            if (terms.size() > 0) {
                Term* f = terms[0];
                nested.push(new BlockTerm(terms, f->line(), f->column()));
            }
            Term* f = nested[0];
            return new BlockTerm(nested, f->line(), f->column());
        }
        else return new BlockTerm(terms, begin.line, begin.column);
    }

    Term* parse(TokenCache::View& view, bool repl) {
        repl_mode = repl;
        Term* t = parseLine(view, view.peek().column);
        if (countErrors() > 0) return nullptr;
        return t;
    }

    ProgramTerm* parseFull(TokenCache::View& view, bool repl) {
        repl_mode = repl;
        ProgramTerm* p = new ProgramTerm({}, view.peek().line, 
                                         view.peek().column);
        while (view.peek()) p->add(parseLine(view, view.peek().column));

        if (countErrors() > 0) {
            return nullptr;
        }

        return p;
    }
}
#include "term.h"
#include "io.h"
#include "value.h"
#include "errors.h"
#include "type.h"

namespace basil {
    template<typename T>
    static Value* factory(const Value* t) {
        return new T(t->line(), t->column());
    }

    TermClass::TermClass(): _parent(nullptr) {
        //
    }

    TermClass::TermClass(const TermClass& parent): _parent(&parent) {
        //
    }

    const TermClass* TermClass::parent() const {
        return _parent;
    }

    // Term

    const TermClass Term::CLASS;
    
    void Term::indent(stream& io, u32 level) const {
        while (level) -- level, print(io, "    ");
    }
    
    void Term::setParent(Term* parent) {
        _parent = parent;
    }

    Term::Term(u32 line, u32 column, const TermClass* tc):
        _line(line), _column(column), _parent(nullptr), _termclass(tc) {
        //
    }

    Term::~Term() {
        //
    }

    u32 Term::line() const {
        return _line;
    }

    u32 Term::column() const {
        return _column;
    }

    // IntegerTerm

    const TermClass IntegerTerm::CLASS(Term::CLASS);

    IntegerTerm::IntegerTerm(i64 value, u32 line, u32 column, 
                                const TermClass* tc):
        Term(line, column, tc), _value(value) {
        //
    }

    i64 IntegerTerm::value() const {
        return _value;
    }

    void IntegerTerm::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Integer ", _value);
    }

    void IntegerTerm::eval(Stack& stack) {
        stack.push(new IntegerConstant(_value, line(), column()));
    }
    
    bool IntegerTerm::equals(const Term* other) const {
        return other->is<IntegerTerm>() 
            && _value == other->as<IntegerTerm>()->_value;
    }

    u64 IntegerTerm::hash() const {
        return ::hash(_value);
    }

    // RationalTerm

    const TermClass RationalTerm::CLASS(Term::CLASS);

    RationalTerm::RationalTerm(double value, u32 line, u32 column,
                                const TermClass* tc):
        Term(line, column, tc), _value(value) {
        //
    }

    double RationalTerm::value() const {
        return _value;
    }

    void RationalTerm::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Rational ", _value);
    }

    void RationalTerm::eval(Stack& stack) {
        stack.push(new RationalConstant(_value, line(), column()));
    }
    
    bool RationalTerm::equals(const Term* other) const {
        return other->is<RationalTerm>() 
            && _value == other->as<RationalTerm>()->_value;
    }

    u64 RationalTerm::hash() const {
        return ::hash(_value);
    }

    // StringTerm

    const TermClass StringTerm::CLASS(Term::CLASS);

    StringTerm::StringTerm(const ustring& value, u32 line, u32 column,
                            const TermClass* tc):
        Term(line, column, tc), _value(value) {
        //
    }

    const ustring& StringTerm::value() const {
        return _value;
    }

    void StringTerm::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "String \"", _value, "\"");
    }

    void StringTerm::eval(Stack& stack) {
        stack.push(new StringConstant(_value, line(), column()));
    }
    
    bool StringTerm::equals(const Term* other) const {
        return other->is<StringTerm>() 
            && _value == other->as<StringTerm>()->_value;
    }

    u64 StringTerm::hash() const {
        return ::hash(_value);
    }

    // CharTerm

    const TermClass CharTerm::CLASS(Term::CLASS);

    CharTerm::CharTerm(uchar value, u32 line, u32 column,
                        const TermClass* tc):
        Term(line, column, tc), _value(value) {
        //
    }

    uchar CharTerm::value() const {
        return _value;
    }

    void CharTerm::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Char '", _value, "'");
    }

    void CharTerm::eval(Stack& stack) {
        stack.push(new CharConstant(_value, line(), column()));
    }
    
    bool CharTerm::equals(const Term* other) const {
        return other->is<CharTerm>() 
            && _value == other->as<CharTerm>()->_value;
    }

    u64 CharTerm::hash() const {
        return ::hash(_value);
    }

    // VariableTerm

    const TermClass VariableTerm::CLASS(Term::CLASS);

    VariableTerm::VariableTerm(const ustring& name, u32 line, u32 column,
                                const TermClass* tc):
        Term(line, column, tc), _name(name) {
        //
    }

    const ustring& VariableTerm::name() const {
        return _name;
    }

    void VariableTerm::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Variable ", _name);
    }

    void VariableTerm::eval(Stack& stack) {
        stack.push(new Variable(_name, line(), column()));
    }
    
    bool VariableTerm::equals(const Term* other) const {
        return other->is<VariableTerm>() 
            && _name == other->as<VariableTerm>()->_name;
    }

    u64 VariableTerm::hash() const {
        return ::hash(_name) ^ ::hash("var");
    }

    // BlockTerm

    const TermClass BlockTerm::CLASS(Term::CLASS);

    BlockTerm::BlockTerm(const vector<Term*>& children, u32 line, u32 column,
                            const TermClass* tc):
        Term(line, column, tc), _children(children) {
        for (Term* t : _children) t->setParent(this);
    }

    BlockTerm::~BlockTerm() {
        for (Term* t : _children) delete t;
    }

    const vector<Term*>& BlockTerm::children() const {
        return _children;
    }

    void BlockTerm::add(Term* child) {
        _children.push(child);
    }

    void BlockTerm::format(stream& io, u32 level) const {
        indent(io, level);
        println("Block");
        for (const Term* t : _children) t->format(io, level + 1);
    }

    void BlockTerm::eval(Stack& stack) {
        Stack* local = new Stack(&stack);
        for (Term* t : _children) {
            if (local->expectsMeta()) {
                local->push(new Quote(t, t->line(), t->column()));
            }
            else t->eval(*local);
        }
        for (Value* v : *local) stack.push(v);
    }
    
    bool BlockTerm::equals(const Term* other) const {
        if (!other->is<BlockTerm>()) return false;
        if (_children.size() != other->as<BlockTerm>()->_children.size()) 
            return false;
        for (u32 i = 0; i < _children.size(); i ++) {
            if (!_children[i]->equals(other->as<BlockTerm>()->_children[i]))
                return false;
        }
        return true;
    }

    u64 BlockTerm::hash() const {
        u64 h = 0;
        for (Term* t : _children) h ^= t->hash();
        return h;
    }

    // LambdaTerm

    const TermClass LambdaTerm::CLASS(Term::CLASS);

    LambdaTerm::LambdaTerm(Term* arg, Term* ret, u32 line, u32 column,
                const TermClass* tc):
        Term(line, column, tc), _arg(arg), _ret(ret) {
        _arg->setParent(this);
        _ret->setParent(this);
    }

    LambdaTerm::~LambdaTerm() {
        delete _arg;
        delete _ret;
    }

    const Term* LambdaTerm::arg() const {
        return _arg;
    }

    const Term* LambdaTerm::ret() const {
        return _ret;
    }

    Term* LambdaTerm::arg() {
        return _arg;
    }

    Term* LambdaTerm::ret() {
        return _ret;
    }

    void LambdaTerm::format(stream& io, u32 level) const {
        indent(io, level);
        println("Lambda");
        _arg->format(io, level + 1);
        _ret->format(io, level + 1);
    }

    void LambdaTerm::eval(Stack& stack) {
        Stack* self = new Stack(&stack, true);
        Stack* args = new Stack(self, true);
        _arg->eval(*args);
        vector<Value*> vals;
        for (Value* v : *args) vals.push(v);

        Stack* body = new Stack(args);

        catchErrors();
        _ret->eval(*body);
        vector<Value*> bodyvals;
        if (countErrors() > 0) {
            bodyvals.push(new Incomplete(_ret, _ret->line(), _ret->column()));
        }
        else for (Value* v : *body) bodyvals.push(v);
        discardErrors();
        
        Value* lam = bodyvals.size() == 1 ? bodyvals[0]
                   : new Sequence(bodyvals, _ret->line(), _ret->column());

        for (i64 i = i64(vals.size()) - 1; i >= 0; -- i) {
            lam = new Lambda(*args, vals[i], lam,
                             _arg->line(), _arg->column());
        }
        if (!lam->is<Lambda>()) {
            lam = new Lambda(*args, 
                             new Sequence({}, _arg->line(), _arg->column()),
                             lam, _arg->line(), _arg->column());
        }
        stack.push(lam);
    }
    
    bool LambdaTerm::equals(const Term* other) const {
        if (!other->is<LambdaTerm>()) return false;
        if (!_arg->equals(other->as<LambdaTerm>()->_arg)) return false;
        if (!_ret->equals(other->as<LambdaTerm>()->_ret)) return false;
        return true;
    }

    u64 LambdaTerm::hash() const {
        return _arg->hash() ^ _ret->hash();
    }

    // AssignTerm

    const TermClass AssignTerm::CLASS(Term::CLASS);

    AssignTerm::AssignTerm(Term* dst, Term* src, u32 line, u32 column,
                            const TermClass* tc):
        Term(line, column, tc), _dst(dst), _src(src) {
        _dst->setParent(this);
        _src->setParent(this);
    }

    AssignTerm::~AssignTerm() {
        delete _dst;
        delete _src;
    }

    const Term* AssignTerm::dst() const {
        return _dst;
    }

    const Term* AssignTerm::src() const {
        return _src;
    }

    Term* AssignTerm::dst() {
        return _dst;
    }

    Term* AssignTerm::src() {
        return _src;
    }

    void AssignTerm::format(stream& io, u32 level) const {
        indent(io, level);
        println("Assign");
        _dst->format(io, level + 1);
        _src->format(io, level + 1);
    }

    void AssignTerm::eval(Stack& stack) {
        u32 size = stack.size();
        _dst->eval(stack);
        if (stack.size() > size + 1) {
            err(PHASE_TYPE, line(), column(),
                "More than one assignable value on left side of assignment.");
            while (stack.size() > size) stack.pop();
        }
        else if (!stack.top()->lvalue(stack)) {
            err(PHASE_TYPE, line(), column(),
                "Value on left side of assignment is not assignable.");
            while (stack.size() > size) stack.pop();
        }
        else {
            Value* lhs = stack.size() ? stack.pop() : nullptr;
            _src->eval(stack);
            Value* rhs = stack.size() ? stack.pop() : nullptr;
            while (stack.size() > size) stack.pop();
            if (!lhs) {
                err(PHASE_TYPE, line(), column(),
                    "No left-hand side value provided to assignment.");
                return;
            }
            if (!rhs) {
                err(PHASE_TYPE, line(), column(),
                    "No right-hand side value provided to assignment.");
                return;
            }

            Assign* assign = new Assign(lhs, rhs, 
                                        lhs->line(), lhs->column());
            assign->apply(stack);
        }
    }
    
    bool AssignTerm::equals(const Term* other) const {
        if (!other->is<AssignTerm>()) return false;
        if (!_src->equals(other->as<AssignTerm>()->_src)) return false;
        if (!_dst->equals(other->as<AssignTerm>()->_dst)) return false;
        return true;
    }

    u64 AssignTerm::hash() const {
        return _src->hash() ^ _dst->hash();
    }

    // ProgramTerm

    const TermClass ProgramTerm::CLASS(Term::CLASS);
    
    void ProgramTerm::initRoot() {
        root->bind("+", BinaryMath::BASE_TYPE(), factory<Add>);
        root->bind("-", BinaryMath::BASE_TYPE(), factory<Subtract>);
        root->bind("*", BinaryMath::BASE_TYPE(), factory<Multiply>);
        root->bind("/", BinaryMath::BASE_TYPE(), factory<Divide>);
        root->bind("%", BinaryMath::BASE_TYPE(), factory<Modulus>);
        root->bind(",", Join::BASE_TYPE(), factory<Join>);
        root->bind("&", Intersect::BASE_TYPE(), factory<Intersect>);
        root->bind("and", BinaryLogic::BASE_TYPE(), factory<And>);
        root->bind("or", BinaryLogic::BASE_TYPE(), factory<Or>);
        root->bind("xor", BinaryLogic::BASE_TYPE(), factory<Xor>);
        root->bind("not", BinaryLogic::BASE_TYPE(), factory<Not>);
        root->bind("==", BinaryEquality::BASE_TYPE(), factory<Equal>);
        root->bind("!=", BinaryEquality::BASE_TYPE(), factory<Inequal>);
        root->bind("<", BinaryRelation::BASE_TYPE(), factory<Less>);
        root->bind("<=", BinaryRelation::BASE_TYPE(), factory<LessEqual>);
        root->bind(">", BinaryRelation::BASE_TYPE(), factory<Greater>);
        root->bind(">=", BinaryRelation::BASE_TYPE(), factory<GreaterEqual>);
        root->bind("print", Print::BASE_TYPE(), factory<Print>);
        root->bind("define", find<FunctionType>(
            META, find<FunctionType>(ANY, ANY)), factory<Autodefine>);
        root->bind("eval", find<FunctionType>(META, ANY), factory<Eval>);

        root->bind("true", new BoolConstant(true, 0, 0));
        root->bind("false", new BoolConstant(false, 0, 0));

        root->bind("i8", new TypeConstant(I8, 0, 0));
        root->bind("i16", new TypeConstant(I16, 0, 0));
        root->bind("i32", new TypeConstant(I32, 0, 0));
        root->bind("i64", new TypeConstant(I64, 0, 0));

        root->bind("u8", new TypeConstant(U8, 0, 0));
        root->bind("u16", new TypeConstant(U16, 0, 0));
        root->bind("u32", new TypeConstant(U32, 0, 0));
        root->bind("u64", new TypeConstant(U64, 0, 0));

        root->bind("f32", new TypeConstant(FLOAT, 0, 0));
        root->bind("f64", new TypeConstant(DOUBLE, 0, 0));
        
        root->bind("char", new TypeConstant(CHAR, 0, 0));
        root->bind("string", new TypeConstant(STRING, 0, 0));

        root->bind("type", new TypeConstant(TYPE, 0, 0));
        root->bind("bool", new TypeConstant(BOOL, 0, 0));
        root->bind("meta", new TypeConstant(META, 0, 0));
    }

    ProgramTerm::ProgramTerm(const vector<Term*>& children, 
                             u32 line, u32 column, const TermClass* tc):
        Term(line, column, tc), _children(children), 
        root(new Stack(nullptr, true)), global(new Stack(root, true)) {
        for (Term* t : _children) t->setParent(this);
        initRoot();
    }

    ProgramTerm::~ProgramTerm() {
        for (Term* t : _children) delete t;
        delete root;
    }

    const vector<Term*>& ProgramTerm::children() const {
        return _children;
    }

    void ProgramTerm::add(Term* child) {
        _children.push(child);
    }

    void ProgramTerm::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Program");
        for (const Term* t : _children) t->format(io, level + 1);
    } 

    void ProgramTerm::eval(Stack& stack) {
        for (Term* t : _children) {
            if (global->expectsMeta()) {
                global->push(new Quote(t, t->line(), t->column()));
            }
            else t->eval(*global);
        }
        for (Value* v : *global) v->type(*global);
        
        vector<Value*> vals;
        for (Value* v : *global) vals.push(v);
        stack.push(new Program(vals, 1, 1)); 
    }

    Stack& ProgramTerm::scope() {
        return *global;
    }

    void ProgramTerm::evalChild(Stack& stack, Term* t) {
        Stack* local = new Stack(global);
        t->eval(*local);
        for (Value* v : *local) v->type(*local);
        stack.copy(*local);
    }
    
    bool ProgramTerm::equals(const Term* other) const {
        if (!other->is<ProgramTerm>()) return false;
        if (_children.size() != other->as<ProgramTerm>()->_children.size()) 
            return false;
        for (u32 i = 0; i < _children.size(); i ++) {
            if (!_children[i]->equals(other->as<ProgramTerm>()->_children[i]))
                return false;
        }
        return true;
    }

    u64 ProgramTerm::hash() const {
        u64 h = 0;
        for (Term* t : _children) h ^= t->hash();
        return h;
    }
}
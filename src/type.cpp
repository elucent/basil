#include "type.h"
#include "io.h"

namespace basil {
    map<ustring, const Type*> typemap;

    TypeClass::TypeClass(): _parent(nullptr) {
        //
    }

    TypeClass::TypeClass(const TypeClass& parent): _parent(&parent) {
        //
    }

    const TypeClass* TypeClass::parent() const {
        return _parent;
    }

    const TypeClass Type::CLASS;

    Type::Type(const ustring& key, u32 size, const TypeClass* tc):
        _typeclass(tc), _key(key), _size(size) {
        //
    }

    u32 Type::size() const {
        return _size;
    }

    bool Type::implicitly(const Type* other) const {
        return other == this || other == ANY;
    }

    bool Type::explicitly(const Type* other) const {
        return other == this || other == ANY;
    }
    
    bool Type::conflictsWith(const Type* other) const {
        return this == other;
    }

    const ustring& Type::key() const {
        return _key;
    }
    
    bool Type::isMeta() const {
        return _size == 0;
    }

    void Type::format(stream& io) const {
        print(io, _key);
    }

    const TypeClass NumericType::CLASS(Type::CLASS);

    NumericType::NumericType(u32 size, bool floating, bool sign, 
                             const TypeClass* tc):
        Type("", size, tc), _floating(floating), _signed(sign) {
        if (floating) _key += "f";
        else _key += sign ? "i" : "u";
        buffer b;
        fprint(b, size * 8);
        while (b) _key += b.read();
    }

    bool NumericType::floating() const {
        return _floating;
    }

    bool NumericType::isSigned() const {
        return _signed;
    }

    bool NumericType::implicitly(const Type* other) const {
        if (Type::implicitly(other)) return true;
        if (!other->is<NumericType>()) return false;
        const NumericType* nt = other->as<NumericType>();

        if (_floating) {
            return nt->_floating && nt->_size >= _size;
        }
        else {
            return !nt->_floating && nt->_signed;
        }
    }

    bool NumericType::explicitly(const Type* other) const {
        if (Type::explicitly(other)) return true;
        return other->is<NumericType>() &&
            (_floating ? other->as<NumericType>()->_floating : true);
    }

    const TypeClass TupleType::CLASS(Type::CLASS);

    TupleType::TupleType(const vector<const Type*>& members,
                const TypeClass* tc):
        Type("", 0, tc), _members(members) {
        _key += "[T";
        for (const Type* t : members) {
            _size += t->size();
            _key += " ", _key += t->key();
        }
        _key += "]";
    }

    const vector<const Type*>& TupleType::members() const {
        return _members;
    }

    const Type* TupleType::member(u32 i) const {
        return _members[i];
    }

    u32 TupleType::count() const {
        return _members.size();
    }

    bool TupleType::implicitly(const Type* other) const {
        if (Type::implicitly(other)) return true;
        if (!other->is<TupleType>()) return false;

        const TupleType* tt = other->as<TupleType>();
        if (tt->_members.size() != _members.size()) return false;

        for (u32 i = 0; i < _members.size(); ++ i) {
            if (!_members[i]->implicitly(tt->_members[i])) return false;
        }

        return true;
    }

    bool TupleType::explicitly(const Type* other) const {
        if (Type::explicitly(other)) return true;
        if (other == TYPE) {
            bool anyNonType = false;
            for (const Type* t : _members) if (t != TYPE) anyNonType = true;
            if (!anyNonType) return true;
        }
        if (!other->is<TupleType>()) return false;

        const TupleType* tt = other->as<TupleType>();
        if (tt->_members.size() != _members.size()) return false;

        for (u32 i = 0; i < _members.size(); ++ i) {
            if (!_members[i]->explicitly(tt->_members[i])) return false;
        }
        
        return true;
    }

    void TupleType::format(stream& io) const {
        print(io, "(");
        bool first = true;
        for (const Type* t : _members) {
            if (!first) print(io, ", ");
            first = false;
            t->format(io);
        }
        print(io, ")");
    }

    bool TupleType::isMeta() const {
        for (const Type* t : _members) {
            if (t->isMeta()) return true;
        }
        return false;
    }

    const TypeClass UnionType::CLASS(Type::CLASS);

    UnionType::UnionType(const set<const Type*>& members,
                         const TypeClass* tc):
        Type("", 0, tc), _members(members) {
        _key += "[U";
        for (const Type* t : members) {
            if (t->size() > _size) _size = t->size();
            _key += " ", _key += t->key();
        }
        _key += "]";
    }

    const set<const Type*>& UnionType::members() const {
        return _members;
    }

    bool UnionType::has(const Type* t) const {
        return _members.find(t) != _members.end();
    }

    bool UnionType::implicitly(const Type* other) const {
        if (Type::implicitly(other)) return true;
        return other == this;
    }

    bool UnionType::explicitly(const Type* other) const {
        if (Type::explicitly(other)) return true;
        if (other == TYPE) {
            bool anyNonType = false;
            for (const Type* t : _members) if (t != TYPE) anyNonType = true;
            if (!anyNonType) return true;
        }
        return has(other);
    }

    void UnionType::format(stream& io) const {
        print(io, "(");
        bool first = true;
        for (const Type* t : _members) {
            if (!first) print(io, " | ");
            first = false;
            t->format(io);
        }
        print(io, ")");
    }

    bool UnionType::isMeta() const {
        for (const Type* t : _members) {
            if (t->isMeta()) return true;
        }
        return false;
    }

    const TypeClass IntersectionType::CLASS(Type::CLASS);

    IntersectionType::IntersectionType(const set<const Type*>& members,
                                       const TypeClass* tc):
        Type("", 0, tc), _members(members) {
        _key += "[I";
        for (const Type* t : members) {
            _size = t->size(); // todo: check that these match
            _key += " ", _key += t->key();
        }
        _key += "]";
    }

    const set<const Type*>& IntersectionType::members() const {
        return _members;
    }

    bool IntersectionType::has(const Type* t) const {
        return _members.find(t) != _members.end();
    }

    bool IntersectionType::implicitly(const Type* other) const {
        if (Type::implicitly(other)) return true;
        return this == other;
    }

    bool IntersectionType::explicitly(const Type* other) const {
        if (Type::explicitly(other)) return true;
        if (other == TYPE) {
            bool anyNonType = false;
            for (const Type* t : _members) if (t != TYPE) anyNonType = true;
            if (!anyNonType) return true;
        }
        return this == other;
    }

    bool IntersectionType::conflictsWith(const Type* other) const {
        for (const Type* t : _members) {
            if (t->conflictsWith(other)) return true;
        }
        return Type::conflictsWith(other);
    }

    void IntersectionType::format(stream& io) const {
        print(io, "(");
        bool first = true;
        for (const Type* t : _members) {
            if (!first) print(io, " & ");
            first = false;
            t->format(io);
        }
        print(io, ")");
    }

    bool IntersectionType::isMeta() const {
        for (const Type* t : _members) {
            if (t->isMeta()) return true;
        }
        return false;
    }

    const TypeClass FunctionType::CLASS(Type::CLASS);

    FunctionType::FunctionType(const Type* arg, const Type* ret,
                               const vector<Constraint>& cons,
                               const TypeClass* tc):
        FunctionType(arg, ret, cons, 
            find<TupleType>(vector<const Type*>()), tc) {
        //
    }

    FunctionType::FunctionType(const Type* arg, const Type* ret, 
                               const vector<Constraint>& cons,
                               const TupleType* capture, 
                               const TypeClass* tc):
        Type("", 0, tc), _arg(arg), _ret(ret), 
        _cons(cons), _capture(capture) {
        _key += "[F ";
        _key += arg->key();
        _key += " ";
        _key += ret->key();
        _key += " { ";
        for (auto& con : cons) _key += con.key() + " ";
        _key += "} ";
        _key += capture->key();
        _key += "]";
        _size = 8 + capture->size();
    }

    const Type* FunctionType::FunctionType::arg() const {
        return _arg;
    }

    const Type* FunctionType::ret() const {
        return _ret;
    }

    const vector<Constraint>& FunctionType::constraints() const {
        return _cons;
    }

    bool FunctionType::conflictsWith(const Type* other) const {
        if (!other->is<FunctionType>()) return false;
        const FunctionType* ft = other->as<FunctionType>();
        if (ft->_arg != _arg && ft->_ret != _ret) return false; 
        for (auto& a : _cons) for (auto& b : ft->_cons) {
            if (a.conflictsWith(b)) return true;
        }
        return false;
    }

    const TupleType* FunctionType::capture() const {
        return _capture;
    }

    bool FunctionType::implicitly(const Type* other) const {
        if (Type::implicitly(other)) return true;
        if (!other->is<FunctionType>()) return false;

        const FunctionType* ft = other->as<FunctionType>();

        if (ft->_ret != _ret || ft->_arg != _arg) return false;
        return ft->_capture->count() == 0 || _capture == ft->_capture;
    }

    bool FunctionType::explicitly(const Type* other) const {
        if (Type::explicitly(other)) return true;
        if (other == TYPE) {
            return _arg == TYPE && _ret == TYPE && _cons.size() == 1
                && _cons[0].type() == EQUALS_VALUE 
                && _cons[0].value().isType();
        }
        return implicitly(other);
    }

    void FunctionType::format(stream& io) const {
        print(io, "(");
        if (_cons.size() == 1 && _cons[0].type() == EQUALS_VALUE) {
            print(io, _cons[0].value().toString());
        }
        else _arg->format(io);
        print(io, " -> ");
        _ret->format(io);
        print(io, ")");
    }

    Constraint FunctionType::matches(FoldResult fr) const {
        return maxMatch(_cons, fr);
    }

    bool FunctionType::isMeta() const {
        return _arg->isMeta() || _ret->isMeta();
    }

    Constraint maxMatch(const vector<Constraint>& cons, FoldResult fr) {
        if (cons.size() == 0) return Constraint();
        Constraint ret = Constraint::NONE;
        for (const Constraint& c : cons) if (c.matches(fr)) {
            if (c.precedes(ret) || ret.type() == UNKNOWN
                || ret.type() == NULL_CONSTRAINT) ret = c;
        }
        return ret;
    }

    // Constraint
    
    Constraint::Constraint(ConstraintType type):
        _type(type) {
        _key = "";
    }

    Constraint::Constraint(FoldResult value):
        _type(EQUALS_VALUE), _value(value) {
        _key = "(= ";
        _key += value.toString();
        _key += ")";
    }

    Constraint::Constraint(const Type* type):
        _type(OF_TYPE), _value(FoldResult(type)) {
        _key = "(: ";
        _key += type->key();
        _key += ")";
    }

    Constraint::Constraint():
        _type(UNKNOWN) {
        _key = "(?)";
    }

    const Constraint Constraint::NONE(NULL_CONSTRAINT);
    
    ConstraintType Constraint::type() const {
        return _type;
    }

    FoldResult Constraint::value() const {
        return _value;
    }

    Constraint::operator bool() const {
        return _type != NULL_CONSTRAINT;
    }

    bool Constraint::conflictsWith(const Constraint& other) const {
        if (_type == UNKNOWN) return true;
        if (_type == other._type && _type == EQUALS_VALUE) {
            return _value == other._value;
        }
        if (_type == OF_TYPE) return other._type == OF_TYPE;
        return false;
    }

    bool Constraint::precedes(const Constraint& other) const {
        return _type < other._type;
    }

    const ustring& Constraint::key() const {
        return _key;
    }

    bool Constraint::matches(FoldResult value) const {
        if (_type == UNKNOWN || _type == OF_TYPE) return true;
        if (_type == EQUALS_VALUE) return value == _value;
        return false;
    }
    
    const Type* join(const Type* a, const Type* b) {
        if (a == b) return a;
        else if (a->implicitly(b)) return b;
        else if (b->implicitly(a)) return a;
        else if (a->explicitly(b)) return b;
        else if (b->explicitly(a)) return a;
        else return nullptr;
    }

    const Type *I8 = find<NumericType>(1, false, true), 
               *I16 = find<NumericType>(2, false, true), 
               *I32 = find<NumericType>(4, false, true), 
               *I64 = find<NumericType>(8, false, true), 
               *U8 = find<NumericType>(1, false, false), 
               *U16 = find<NumericType>(2, false, false), 
               *U32 = find<NumericType>(4, false, false), 
               *U64 = find<NumericType>(8, false, false), 
               *FLOAT = find<NumericType>(4, true, true), 
               *DOUBLE = find<NumericType>(8, true, true),
               *BOOL = find<Type>("bool", 1), 
               *TYPE = find<Type>("type", 0), 
               *META = find<Type>("meta", 0),
               *ERROR = find<Type>("error", 1),
               *VOID = find<Type>("void", 1),
               *ANY = find<Type>("any", 1),
               *UNDEFINED = find<Type>("undefined", 1),
               *STRING = find<Type>("string", 8),
               *CHAR = find<Type>("char", 4);
}

void print(stream& io, const basil::Type* t) {
    t->format(io);
}

void print(const basil::Type* t) {
    print(_stdout, t);
}
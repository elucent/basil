#include "value.h"
#include "io.h"
#include "type.h"
#include "term.h"
#include "errors.h"
#include "ir.h"

namespace basil {

    // Stack

    Stack::Entry::Entry(const Type* type, Stack::builtin_t builtin):
        _type(type), _builtin(builtin), _reassigned(false) {
        //
    }

    Stack::Entry::Entry(const Type* type, FoldResult value, builtin_t builtin):
        _type(type), _value(value), _builtin(builtin), _reassigned(false) {
        //
    }
    
    const Type* Stack::Entry::type() const {
        return _type;
    }

    Stack::builtin_t Stack::Entry::builtin() const {
        return _builtin;
    }

    FoldResult Stack::Entry::value() const {
        return _value;
    }

    Location* Stack::Entry::loc() const {
        return _loc;
    }
    
    const Type*& Stack::Entry::type() {
        return _type;
    }

    Stack::builtin_t& Stack::Entry::builtin() {
        return _builtin;
    }

    FoldResult& Stack::Entry::value() {
        return _value;
    }

    Location*& Stack::Entry::loc() {
        return _loc;
    }

    bool Stack::Entry::reassigned() const {
        return _reassigned;
    }

    void Stack::Entry::reassign() {
        _reassigned = true;
    }

    const FunctionType* Stack::tryVoid(Value* func) {
        const Type* t = func->type(*this);
        if (t->is<FunctionType>()) {
            const FunctionType* ft = t->as<FunctionType>();
            if (ft->arg() == VOID) return ft;
            return nullptr;
        }
        else if (t->is<IntersectionType>()) {
            const IntersectionType* it = t->as<IntersectionType>();
            vector<const FunctionType*> fns;
            for (const Type* t : it->members()) {
                if (t->is<FunctionType>() 
                    && t->as<FunctionType>()->arg() == VOID) {
                    fns.push(t->as<FunctionType>());
                }
            }
            if (fns.size() > 1) {
                buffer b;
                fprint(b, "Ambiguous application of overloaded function. ",
                       "Candidates were:");
                for (const FunctionType* fn : fns) {
                    fprint(b, '\n');
                    fprint(b, "    ", (const Type*)fn);
                }
                err(PHASE_TYPE, func->line(), func->column(), b);   
            }
            else if (fns.size() == 1) {
                return fns[0];
            }
            else return nullptr;
        }
        return nullptr;
    }

    const FunctionType* Stack::tryApply(const Type* func, Value* arg,
                                        u32 line, u32 column) {
        const Type* t = func;
        const Type* argt = arg->type(*this);
        if (t->is<FunctionType>()) {
            const FunctionType* ft = t->as<FunctionType>();
            if (argt->explicitly(ft->arg())) {
                return ft;
            }
            else return nullptr;
        }
        if (t->is<IntersectionType>()) {
            const IntersectionType* it = t->as<IntersectionType>();
            vector<const FunctionType*> fns;
            for (const Type* t : it->members()) {
                if (auto ft = tryApply(t, arg, line, column)) {
                    fns.push(ft);
                }
            }
            if (fns.size() > 1) {
                bool equalFound = false;
                bool implicitFound = false;
                for (const FunctionType* ft : fns) {
                    if (argt == ft->arg()) equalFound = true;
                    if (argt->implicitly(ft->arg())) implicitFound = true;
                }

                if (equalFound) {
                    const FunctionType** first = fns.begin();
                    const FunctionType** iter = first;
                    u32 newsize = 0;
                    while (iter != fns.end()) {
                        if (argt == (*iter)->arg()) {
                            *(first ++) = *iter, ++ newsize;
                        }
                        ++ iter;
                    }
                    while (fns.size() > newsize) fns.pop();
                }
                else if (implicitFound) {
                    const FunctionType** first = fns.begin();
                    const FunctionType** iter = first;
                    u32 newsize = 0;
                    while (iter != fns.end()) {
                        if (argt->implicitly((*iter)->arg())) {
                            *(first ++) = *iter, ++ newsize;
                        }
                        ++ iter;
                    }
                    while (fns.size() > newsize) fns.pop();
                }
            }

            if (fns.size() > 1) {
                buffer b;
                fprint(b, "Ambiguous application of overloaded function ",
                         "for argument type '", argt, "'. Candidates were:");
                for (const FunctionType* fn : fns) {
                    fprint(b, '\n');
                    fprint(b, "    ", (const Type*)fn);
                }
                err(PHASE_TYPE, line, column, b);
            }
            else if (fns.size() == 1) {
                return fns[0];
            }
            else return nullptr;
        }
        return nullptr;
    }

    const FunctionType* Stack::tryApply(Value* func, Value* arg) {
        return tryApply(func->type(*this), arg, func->line(), func->column());
    }

    Value* Stack::apply(Value* func, const FunctionType* ft, Value* arg) {
        auto e = func->entry(*this);
        if (e && e->builtin()) func = e->builtin()(func);
        if (func->is<Builtin>() && func->as<Builtin>()->canApply()) {
            return func->as<Builtin>()->apply(*this, arg);
        }
        else {
            if (!func->type(*this)->implicitly(ft)) func = new Cast(ft, func);
            return new Call(func, arg, func->line(), func->column());
        }
    }
    
    Stack::Stack(Stack* parent, bool scope): 
        _parent(parent),
        table(scope ? new map<ustring, Entry>() : nullptr) {
        if (parent) parent->_children.push(this);
    }

    Stack::~Stack() {
        if (table) delete table;
        for (Stack* s : _children) delete s;
    }

    Stack::Stack(const Stack& other): 
        _parent(other._parent), values(other.values) {
        if (other.table) table = new map<ustring, Entry>(*other.table);
        else table = nullptr;
        for (Stack* s : other._children) {
            _children.push(new Stack(*s));
        }
    }

    Stack& Stack::operator=(const Stack& other) {
        if (this != &other) {
            if (table) delete table;
            for (Stack* s : _children) delete s;
            _parent = other._parent;
            values = other.values;
            if (other.table) table = new map<ustring, Entry>(*other.table);
            else table = nullptr;
            for (Stack* s : other._children) {
                _children.push(new Stack(*s));
            }
        }
        return *this;
    }

    void Stack::detachTo(Stack& s) {
        table = nullptr;
        for (Stack* c : _children) {
            c->_parent = &s;
            s._children.push(c);
        }
        _children.clear();
    }

    const Value* Stack::top() const {
        return values.back();
    }

    Value* Stack::top() {
        return values.back();
    }

    const Value* const* Stack::begin() const {
        return values.begin();
    }

    Value** Stack::begin() {
        return values.begin();
    }

    const Value* const* Stack::end() const {
        return values.end();
    }

    Value** Stack::end() {
        return values.end();
    }
    
    bool Stack::expectsMeta() {
        if (size() == 0) return false;
        // eval does not quote things
        if (top()->is<Eval>()) return false;
        if (top()->entry(*this) && top()->entry(*this)->builtin()
            && top()->entry(*this)->builtin()(top())->is<Eval>()) {
            return false;
        }

        const Type* tt = top()->type(*this);
        if (tt->is<FunctionType>()) {
            return tt->as<FunctionType>()->arg() == META;
        }
        else if (tt->is<IntersectionType>()) {
            for (const Type* t : tt->as<IntersectionType>()->members()) {
                if (t->is<FunctionType>()
                    && t->as<FunctionType>()->arg() == META) return true;
            }
        }
        return false;
    }

    void Stack::push(Value* v) {
        if (!v) return;
        // try declaration
        if (values.size() && top()->type(*this)->explicitly(TYPE) 
            && v->is<Variable>() 
            && (!v->entry(*this) || !tryApply(v, top()))) {
            if (top()->type(*this) != TYPE) {
                values.push(new Cast(TYPE, pop()));
            }
            Define* d = new Define(pop(), v->as<Variable>()->name());
            d->apply(*this, nullptr);
            return push(d);
        }

        // try function application
        if (!values.size()) {
            values.push(v);
            return;
        }
        else if (const FunctionType* ft = tryApply(top(), v)) {
            if (v->type(*this) != ft->arg() && ft->arg() != ANY) {
                v = new Cast(ft->arg(), v);
            }
            Value* result = apply(pop(), ft, v);
            if (result) push(result);
        }
        else if (const FunctionType* ft = tryApply(v, top())) {
            if (top()->type(*this) != ft->arg() && ft->arg() != ANY) {
                values.back() = new Cast(ft->arg(), top());
            }
            Value* result = apply(v, ft, pop());
            if (result) push(result);
        }
        else values.push(v);

        // try void functions
        if (!values.size()) return;
        // if (auto ft = tryVoid(top())) {
        //     Value* v = pop();
        //     Value* result = apply(v, ft, nullptr);
        //     if (result) push(result);
        // }
    }

    Value* Stack::pop() {
        Value* v = values.back();
        values.pop();
        return v;
    }

    bool Stack::hasScope() const {
        return table;
    }

    const Stack::Entry* Stack::operator[](const ustring& name) const {
        if (table) {
            auto it = table->find(name);
            if (it != table->end()) return &(it->second);
        }
        if (_parent) return (*_parent)[name];
        return nullptr;
    }

    Stack::Entry* Stack::operator[](const ustring& name) {
        if (table) {
            auto it = table->find(name);
            if (it != table->end()) return &(it->second);
        }
        if (_parent) return (*_parent)[name];
        return nullptr;
    }

    void Stack::bind(const ustring& name, const Type* t) {
        if (table) (*table)[name] = Entry(t);
        else if (_parent) _parent->bind(name, t);
    }

    void Stack::bind(const ustring& name, const Type* t, builtin_t b) {
        if (table) (*table)[name] = Entry(t, b);
        else if (_parent) _parent->bind(name, t, b);
    }

    void Stack::bind(const ustring& name, Value* v) {
        if (table) (*table)[name] = Entry(v->type(*this), v->fold(*this));
        else if (_parent) _parent->bind(name, v);
    }

    void Stack::erase(const ustring& name) {
        if (table) table->erase(name);
    }

    void Stack::clear() {
        values.clear();
    }

    void Stack::copy(Stack& other) {
        for (Value* v : other) values.push(v);
    }

    void Stack::copy(vector<Value*>& other) {
        for (Value* v : other) values.push(v);
    }

    u32 Stack::size() const {
        return values.size();
    }
    
    const map<ustring, Stack::Entry>& Stack::nearestScope() const {
        const Stack* s = this;
        const map<ustring, Stack::Entry>* t = table;
        while (s->_parent && !t) {
            s = s->_parent;
            t = s->table;
        }
        return *t;
    }
    
    const map<ustring, Stack::Entry>& Stack::scope() const {
        return *table;
    }
    
    map<ustring, Stack::Entry>& Stack::scope() {
        return *table;
    }
    
    Stack* Stack::parent() {
        return _parent;
    }

    const Stack* Stack::parent() const {
        return _parent;
    }

    // FoldResult

    const u8 FoldResult::TYPE_NULL = 0, 
             FoldResult::TYPE_INT = 1, 
             FoldResult::TYPE_DOUBLE = 2, 
             FoldResult::TYPE_TYPE = 3, 
             FoldResult::TYPE_TERM = 4,
             FoldResult::TYPE_FUNCTION = 5,
             FoldResult::TYPE_BOOL = 6;

    FoldResult::FoldResult(): _type(TYPE_NULL) {
        //
    }

    FoldResult::FoldResult(i64 i): _type(TYPE_INT) {
        value.i = i;
    }

    FoldResult::FoldResult(double d): _type(TYPE_DOUBLE) {
        value.d = d;
    }

    FoldResult::FoldResult(bool b): _type(TYPE_BOOL) {
        value.b = b;
    }

    FoldResult::FoldResult(const Type* t): _type(TYPE_TYPE) {
        value.t = t;
    }

    FoldResult::FoldResult(Term* v): _type(TYPE_TERM) {
        value.v = v;
    }

    FoldResult::FoldResult(Value* f): _type(TYPE_FUNCTION) {
        value.f = f;
    }

    bool FoldResult::isNumeric() const {
        return isInt() || isFloat();
    }

    bool FoldResult::isInt() const {
        return _type == TYPE_INT;
    }

    bool FoldResult::isFloat() const {
        return _type == TYPE_DOUBLE;
    }

    bool FoldResult::isBool() const {
        return _type == TYPE_BOOL;
    }

    bool FoldResult::isType() const {
        return _type == TYPE_TYPE;
    }

    bool FoldResult::isTerm() const {
        return _type == TYPE_TERM;
    }

    bool FoldResult::isFunction() const {
        return _type == TYPE_FUNCTION;
    }

    i64 FoldResult::asInt() const {
        return value.i;
    }

    i64& FoldResult::asInt() {
        return value.i;
    }

    double FoldResult::asFloat() const {
        return value.d;
    }

    double& FoldResult::asFloat() {
        return value.d;
    }

    bool FoldResult::asBool() const {
        return value.b;
    }

    bool& FoldResult::asBool() {
        return value.b;
    }

    const Type* FoldResult::asType() const {
        return value.t;
    }

    const Type*& FoldResult::asType() {
        return value.t;
    }

    Term* FoldResult::asTerm() const {
        return value.v;
    }

    Term*& FoldResult::asTerm() {
        return value.v;
    }

    const Value* FoldResult::asFunction() const {
        return value.f;
    }

    Value* FoldResult::asFunction() {
        return value.f;
    }

    bool FoldResult::operator==(FoldResult fr) const {
        if (fr._type != _type) return false;
        if (fr.isInt()) return fr.asInt() == asInt();
        else if (fr.isType()) return fr.asType() == asType();
        else if (fr.isFloat()) return fr.asFloat() == asFloat();
        else if (fr.isFunction()) return fr.asFunction() == asFunction();
        else if (fr.isBool()) return fr.asBool() == asBool();
        else if (fr.isTerm()) return fr.asTerm()->equals(asTerm());
        return false;
    }

    bool FoldResult::operator!=(FoldResult fr) const {
        return !operator==(fr);
    }

    FoldResult::operator bool() const {
        return _type != TYPE_NULL;
    }

    ustring FoldResult::toString() const {
        buffer b;
        if (_type == TYPE_INT) fprint(b, asInt());
        if (_type == TYPE_DOUBLE) fprint(b, asFloat());
        if (_type == TYPE_TYPE) fprint(b, asType()->key());
        if (_type == TYPE_BOOL) fprint(b, asBool() ? "true" : "false");
        if (_type == TYPE_TERM) fprint(b, "[term]");
        ustring s;
        fread(b, s);
        return s;
    }

    u64 hash(FoldResult fr) {
        if (fr.isInt()) return ::hash(fr.asInt());
        else if (fr.isType()) return ::hash(fr.asType());
        else if (fr.isFloat()) return ::hash(fr.asFloat());
        else if (fr.isFunction()) return ::hash(fr.asFunction());
        else if (fr.isBool()) return ::hash(fr.asBool());
        else if (fr.isTerm()) return fr.asTerm()->hash();
        return 0;
    }
    
    Location* FoldResult::gen(CodeGenerator& gen, CodeFrame& frame) {
        if (isInt()) return frame.add(new IntData(asInt()))->value(gen, frame);
        return frame.none();
    }

    // ValueClass

    ValueClass::ValueClass(): _parent(nullptr) {
        //
    }

    ValueClass::ValueClass(const ValueClass& parent): _parent(&parent) {
        //
    }

    const ValueClass* ValueClass::parent() const {
        return _parent;
    }

    // Value

    const ValueClass Value::CLASS;

    void Value::indent(stream& io, u32 level) const {
        while (level) -- level, print(io, "    ");
    }

    void Value::setType(const Type* t) {
        _cachetype = t;
    }

    const Type* Value::lazyType(Stack& ctx) {
        return ERROR;
    }

    void Value::updateType(Stack& ctx) {
        setType(lazyType(ctx));
    }

    Value::Value(u32 line, u32 column, const ValueClass* vc):
        _line(line), _column(column), _valueclass(vc), _cachetype(nullptr) {
        //
    }

    Value::~Value() {
        //
    }

    u32 Value::line() const {
        return _line;
    }

    u32 Value::column() const {
        return _column;
    }

    FoldResult Value::fold(Stack& ctx) {
        return FoldResult();
    }
    
    bool Value::lvalue(Stack& ctx) const {
        return false;
    }
    
    Stack::Entry* Value::entry(Stack& ctx) const {
        return nullptr;
    }

    const Type* Value::type(Stack& ctx) {
        if (!_cachetype) setType(lazyType(ctx));
        return _cachetype;
    }

    Location* Value::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        return frame.none();
    }

    // Builtin

    const ValueClass Builtin::CLASS(Value::CLASS);

    Builtin::Builtin(u32 line, u32 column, const ValueClass* vc):
        Value(line, column, vc) {
        //
    }

    bool Builtin::canApply() const {
        return true;
    }

    // IntegerConstant

    const ValueClass IntegerConstant::CLASS(Value::CLASS);

    IntegerConstant::IntegerConstant(i64 value, u32 line, u32 column, 
                                     const ValueClass* tc):
        Value(line, column, tc), _value(value) {
        setType(I64);
    }

    i64 IntegerConstant::value() const {
        return _value;
    }

    void IntegerConstant::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Integer ", _value);
    }

    FoldResult IntegerConstant::fold(Stack& ctx) {
        return FoldResult(_value);
    }
    
    Location* IntegerConstant::gen(Stack& ctx, 
        CodeGenerator& gen, CodeFrame& frame) {
        return frame.add(new IntData(_value))->value(gen, frame);
    }

    // RationalConstant

    const ValueClass RationalConstant::CLASS(Value::CLASS);

    RationalConstant::RationalConstant(double value, u32 line, u32 column, 
                                       const ValueClass* tc):
        Value(line, column, tc), _value(value) {
        setType(DOUBLE);
    }

    double RationalConstant::value() const {
        return _value;
    }

    void RationalConstant::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Float ", _value);
    }

    FoldResult RationalConstant::fold(Stack& ctx) {
        return FoldResult(_value);
    }

    // StringConstant

    const ValueClass StringConstant::CLASS(Value::CLASS);

    StringConstant::StringConstant(const ustring& value, u32 line, u32 column, 
                    const ValueClass* vc):
        Value(line, column, vc), _value(value) {
        setType(STRING);
    }

    const ustring& StringConstant::value() const {
        return _value;
    }

    void StringConstant::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "String \"", escape(_value), "\"");
    }

    FoldResult StringConstant::fold(Stack& ctx) {
        return FoldResult();
        // TODO string foldresult
    }

    Location* StringConstant::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        return frame.add(new StrData(_value))->value(gen, frame);
    }

    // CharConstant

    const ValueClass CharConstant::CLASS(Value::CLASS);

    CharConstant::CharConstant(uchar value, u32 line, u32 column, 
                    const ValueClass* vc):
        Value(line, column, vc), _value(value) {
        setType(CHAR);
    }

    uchar CharConstant::value() const {
        return _value;
    }

    void CharConstant::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Character '", _value, "'");
    }

    FoldResult CharConstant::fold(Stack& ctx) {
        return FoldResult();
        // TODO char foldresult
    }

    // TypeConstant

    const ValueClass TypeConstant::CLASS(Value::CLASS);

    TypeConstant::TypeConstant(const Type* value, u32 line, u32 column,
                               const ValueClass* vc):
        Value(line, column, vc), _value(value) {
        setType(TYPE);
    }

    const Type* TypeConstant::value() const {
        return _value;
    }

    void TypeConstant::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Type ", _value->key());
    }

    FoldResult TypeConstant::fold(Stack& ctx) {
        return FoldResult(_value);
    }

    // BoolConstant

    const ValueClass BoolConstant::CLASS(Value::CLASS);

    BoolConstant::BoolConstant(bool value, u32 line, u32 column,
                    const ValueClass* vc):
        Value(line, column, vc), _value(value) {
        setType(BOOL);
    }

    bool BoolConstant::value() const {
        return _value;
    }

    void BoolConstant::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Boolean ", _value);
    }

    FoldResult BoolConstant::fold(Stack& ctx) {
        return FoldResult(_value);
    }

    // Quote

    const ValueClass Quote::CLASS(Value::CLASS);

    Quote::Quote(Term* term, u32 line, u32 column,
                 const ValueClass* vc):
        Value(line, column, vc), _term(term) {
        setType(META);
    }

    Term* Quote::term() const {
        return _term;
    }

    void Quote::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Quote");
        _term->format(io, level + 1);
    }

    FoldResult Quote::fold(Stack& ctx) {
        return FoldResult(_term);
    }

    // Incomplete

    const ValueClass Incomplete::CLASS(Value::CLASS);

    Incomplete::Incomplete(Term* term, u32 line, u32 column,
                const ValueClass* vc):
        Value(line, column, vc), _term(term) {
        setType(ANY);
    }
    
    Term* Incomplete::term() const {
        return _term;
    }

    void Incomplete::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Incomplete");
        _term->format(io, level + 1);
    }

    // Variable

    const ValueClass Variable::CLASS(Value::CLASS);

    const Type* Variable::lazyType(Stack& ctx) {
        const Stack::Entry* entry = ctx[_name];
        if (!entry) {
            err(PHASE_TYPE, line(), column(),
                "Undeclared variable '", _name, "'.");
            return ERROR;
        }
        return entry->type();
    }

    Variable::Variable(const ustring& name, u32 line, u32 column,
                       const ValueClass* vc):
        Value(line, column, vc), _name(name) {
        //
    }

    const ustring& Variable::name() const {
        return _name;
    }

    void Variable::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Variable ", _name);
    }

    FoldResult Variable::fold(Stack& ctx) {
        const Stack::Entry* entry = ctx[_name];
        if (!entry) {
            err(PHASE_TYPE, line(), column(),
                "Undeclared variable '", _name, "'.");
            return {};
        }
        FoldResult fr = entry->value();
        return fr;
    }

    bool Variable::lvalue(Stack& ctx) const {
        return true;
    }

    Stack::Entry* Variable::entry(Stack& ctx) const {
        return ctx[_name];
    }
    
    Location* Variable::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        return entry(ctx)->loc();
    }

    // Sequence

    const ValueClass Sequence::CLASS(Value::CLASS);

    const Type* Sequence::lazyType(Stack& ctx) {
        if (!_children.size()) return VOID;
        else return _children.back()->type(ctx);
    }

    Sequence::Sequence(const vector<Value*>& children, u32 line, u32 column,
                const ValueClass* vc):
        Value(line, column, vc), _children(children) {
    }

    Sequence::~Sequence() {
        for (Value* v : _children) if (v) delete v;
    }

    const vector<Value*>& Sequence::children() const {
        return _children;
    }

    void Sequence::append(Value* v) {
        _children.push(v);
    }

    void Sequence::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Sequence");
        for (Value* v : _children) v->format(io, level + 1);
    }

    FoldResult Sequence::fold(Stack& ctx) {
        return FoldResult(); // TODO: sequence fold check
    }
    
    Location* Sequence::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        Location* loc = frame.none();
        for (Value* v : _children) loc = v->gen(ctx, gen, frame);
        return loc;
    }

    // Program
    
    const ValueClass Program::CLASS(Value::CLASS);

    const Type* Program::lazyType(Stack& ctx) {
        if (!_children.size()) return VOID;
        else return _children.back()->type(ctx);
    }

    Program::Program(const vector<Value*>& children, u32 line, u32 column,
                const ValueClass* vc):
        Value(line, column, vc), _children(children) {
    }

    Program::~Program() {
        for (Value* v : _children) if (v) delete v;
    }

    const vector<Value*>& Program::children() const {
        return _children;
    }

    void Program::append(Value* v) {
        _children.push(v);
    }

    void Program::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Program");
        for (Value* v : _children) v->format(io, level + 1);
    }

    FoldResult Program::fold(Stack& ctx) {
        return FoldResult(); // TODO: sequence fold check
    }
    
    Location* Program::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        Location* loc = frame.none();
        for (Value* v : _children) loc = v->gen(ctx, gen, frame);
        return loc;
    }

    // Lambda

    const ValueClass Lambda::CLASS(Value::CLASS);

    const Type* Lambda::lazyType(Stack& ctx) {
        Stack& s = _ctx ? *_ctx : ctx;
        const Type* mt = _match->type(s);

        catchErrors();
        const Type* bt = _body->type(s);
        if (countErrors()) bt = ANY;
        discardErrors();

        if (_match->is<Define>() || _match->is<Autodefine>()) {
            return find<FunctionType>(mt, bt, vector<Constraint>({
                Constraint(mt)
            }));
        }
        else if (auto fr = _match->fold(s)) {
            return find<FunctionType>(mt, bt, vector<Constraint>({
                Constraint(fr)
            }));
        }
        else return find<FunctionType>(mt, bt);
    }
        
    Lambda::Lambda(Value* match, Value* body, 
                   u32 line, u32 column, const ValueClass* vc):
        Value(line, column, vc), _ctx(nullptr), 
        _body(body), _match(match), _inlined(false) {
        //
    }
        
    Lambda::Lambda(Stack& ctx, Value* match, Value* body, 
                   u32 line, u32 column, const ValueClass* vc):
        Value(line, column, vc), _ctx(&ctx), 
        _body(body), _match(match), _inlined(false) {
        //
    }
        
    Lambda::Lambda(Stack* ctx, Value* match, Value* body, 
                   u32 line, u32 column, const ValueClass* vc):
        Value(line, column, vc), _ctx(ctx), 
        _body(body), _match(match), _inlined(false) {
        //
    }

    Lambda::~Lambda() {
        if (_match) delete _match;
        if (_body) delete _body;
    }

    Value* Lambda::body() {
        return _body;
    }

    Value* Lambda::match() {
        return _match;
    }

    Stack* Lambda::scope() {
        return _ctx;
    }

    Stack* Lambda::self() {
        return _ctx->parent();
    }

    Value* Lambda::expand(Stack* s) {
        if (!_body->is<Incomplete>()) return this;

        Stack* new_args = s;
        Stack* body_scope = new Stack(new_args);
        _body->as<Incomplete>()->term()->eval(*body_scope);
        if (countErrors()) {
            printErrors(_stdout);
            return this;
        }
        vector<Value*> bodyvals;
        for (Value* v : *body_scope) bodyvals.push(v);
        Value* body = bodyvals.size() == 1 ? bodyvals[0]
            : new Sequence(bodyvals, line(), column());
        return new Lambda(new_args, _match, body, line(), column());
    }
        
    void Lambda::format(stream& io, u32 level) const {
        indent(io, level);
        println("Lambda");
        _match->format(io, level + 1);
        _body->format(io, level + 1);
    }

    FoldResult Lambda::fold(Stack& ctx) {
        return FoldResult(this);
    }

    void Lambda::bindrec(const ustring& name, const Type* type,
                          FoldResult value) {
        self()->bind(name, type);
        self()->operator[](name)->value() = value;
        setType(
            find<FunctionType>(
                type->as<FunctionType>()->arg(),
                type->as<FunctionType>()->ret(),
                this->type(*_ctx)->as<FunctionType>()->constraints()
            )
        );
        if (_body->is<Incomplete>()) {
            catchErrors();
            Stack* body = new Stack(_ctx);
            _body->as<Incomplete>()->term()->eval(*body);
            if (countErrors() == 0) {
                vector<Value*> bodyvals;
                for (Value* v : *body) bodyvals.push(v);
                Value* lam = bodyvals.size() == 1 ? bodyvals[0]
                        : new Sequence(bodyvals, _body->line(), _body->column());
                delete _body;
                _body = lam;
            }
            discardErrors();
        }
    }

    void Lambda::saveExpansion(FoldResult arg, Value* expander) {
        expansions.put(arg, expander);
    }

    Value* Lambda::expansionFor(FoldResult arg) {
        auto it = expansions.find(arg);
        if (it != expansions.end()) return it->second;
        return nullptr;
    }
    
    Location* Lambda::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        if (type(ctx)->isMeta()) return frame.none();

        if (!_label.size()) {
            Function* func = gen.newFunction();
            _label = func->label();
            for (const ustring& label : _alts) 
                func->add(new Label(label, true));
            if (auto e = _match->entry(*_ctx)) {
                e->loc() = func->stack(_match->type(*_ctx));
                func->add(new MovInsn(e->loc(),
                    gen.locateArg(_match->type(*_ctx))));
            }
                // e->loc() = Location(STACK, 16, _match->type(*_ctx));
            Location* retval = _body->gen(*_ctx, gen, *func);
            if (type(ctx)->as<FunctionType>()->ret() != VOID) 
                func->add(new RetInsn(retval));
        }
        Location* loc = frame.stack(type(ctx));
        frame.add(new LeaInsn(loc, _label));
        return loc;
    }

    bool Lambda::inlined() const {
        return _inlined;
    }
    
    Location* Lambda::genInline(Stack& ctx, Location* arg, CodeGenerator& gen, CodeFrame& frame) {
        _inlined = true;
        if (auto e = _match->entry(*_ctx)) {
            e->loc() = arg;
        }
        Location* retval = _body->gen(*_ctx, gen, frame);
        if (type(ctx)->as<FunctionType>()->ret() == VOID) 
            retval = frame.none();
        return retval;
    }

    const ustring& Lambda::label() const {
        return _label;
    }
    
    void Lambda::addAltLabel(const ustring& label) {
        _alts.push(label);
    }

    // Call
        
    const ValueClass Call::CLASS(Value::CLASS);

    Lambda* caseFor(Stack& ctx, Value* fn, Value* arg) {
        if (fn->is<Lambda>()) return fn->as<Lambda>();
        if (fn->is<Intersect>()) {
            FoldResult a = arg->fold(ctx);
            return fn->as<Intersect>()->caseFor(ctx, a);
        }
        return nullptr;
    }

    void Call::expand(Stack& ctx) {
        FoldResult fr = _func->fold(ctx);
        if (_func->is<Call>() && _func->as<Call>()->expanded) {
            fr = _func->as<Call>()->expanded->fold(
                *_func->as<Call>()->env
            );
        }
        Value* fn = fr.asFunction();
        Lambda* l = caseFor(ctx, fn, _arg);
        if (!l) {
            err(PHASE_TYPE, line(), column(),
                "Could not determine expansion for call.");
            return;
        }

        FoldResult arg = _arg->fold(ctx);
        if (auto e = l->expansionFor(arg)) {
            expanded = e->as<Call>()->expanded;
            env = e->as<Call>()->env;

            if (!expanded) recursiveMacro = true;
            return;
        }
        else l->saveExpansion(arg, this);

        Stack* env_src = l->scope();
        env = new Stack(env_src->parent(), true);
        env->scope() = env_src->scope();
        l->match()->entry(*env)->value() = arg;
        
        // if (arg) println(_stdout, "argument value in call = ", arg.toString());
        // println(_stdout, "arg = ");
        // _arg->format(_stdout, 1);

        // Stack* s = env;
        // while (s) {
        //     if (s->hasScope()) for (auto &e : s->scope()) {
        //         println(_stdout, e.first, ": ", e.second.type(), "|", e.second.value().toString());
        //     }
        //     println("");
        //     println("");
        //     s = s->parent();
        // }

        Stack* body = new Stack(env);
        if (auto e = l->match()->entry(*body)) {
            e->value() = arg;
            if (auto a = _arg->entry(ctx))
                e->builtin() = a->builtin();
        }
        if (l->body()->is<Incomplete>()) {
            l->body()->as<Incomplete>()->term()->eval(*body);
            vector<Value*> bodyvals;
            for (Value* v : *body) bodyvals.push(v);
            expanded = body->size() == 1 ? bodyvals.back()
                : new Sequence(bodyvals, line(), column());
        }
        else if (l->body()->is<Intersect>()) {
            expanded = l->body()->as<Intersect>()->expand(env);
        }
        else if (l->body()->is<Lambda>()) {
            expanded = l->body()->as<Lambda>()->expand(env);
        }
        else if (l->body()->is<Call>()) {
            // TODO: This is an awful hack! Make a better 
            //       way to recurse through.
            Value*& f = l->body()->as<Call>()->_func;
            if (f->is<Lambda>()) f = f->as<Lambda>()->expand(env);
            if (f->is<Intersect>()) f = f->as<Intersect>()->expand(env);
            expanded = l->body();
        }
        else expanded = l->body();
        // println(_stdout, "Call expanded to: ");
        // expanded->format(_stdout, 1);
    }

    const Type* Call::lazyType(Stack& ctx) {
        const Type* t = _func->type(ctx);
        if (!t->is<FunctionType>()) {
            err(PHASE_TYPE, line(), column(),
                "Called object does not have function type.");
            return ERROR;
        }
        
        if (!expanded &&
            (t->as<FunctionType>()->arg()->isMeta()
             || t->as<FunctionType>()->ret()->isMeta()
             || t->as<FunctionType>()->ret() == ANY)) {
            expand(ctx);
        }

        if (expanded && !recursiveMacro) return expanded->type(*env);
        return t->as<FunctionType>()->ret();
    }

    Call::Call(Value* func, Value* arg, u32 line, u32 column,
               const ValueClass* vc):
        Value(line, column, vc), _func(func), _arg(arg), 
        expanded(nullptr), env(nullptr), recursiveMacro(false) {
        //
    }

    Call::Call(Value* func, u32 line, u32 column,
               const ValueClass* vc):
        Value(line, column, vc), _func(func), _arg(nullptr), 
        expanded(nullptr), env(nullptr), recursiveMacro(false) {
        //
    }

    Call::~Call() {
        delete _func;
        if (_arg) delete _arg;
        if (expanded) delete expanded;
    }

    void Call::format(stream& io, u32 level) const {
        indent(io, level);
        println("Call");
        _func->format(io, level + 1);
        if (_arg) _arg->format(io, level + 1);
    }

    FoldResult Call::fold(Stack& ctx) {
        if (inst) return inst;

        if (!_func->type(ctx)->is<FunctionType>()) {
            err(PHASE_TYPE, line(), column(),
                "Called object does not have function type.");
            return ERROR;
        }

        FoldResult fr = _func->fold(ctx);
        if (_func->is<Call>() && _func->as<Call>()->expanded) {
            fr = _func->as<Call>()->expanded->fold(
                *_func->as<Call>()->env
            );
        }
        if (!fr.isFunction()) {
            err(PHASE_TYPE, line(), column(),
                "Called object does not have function type.");
            return ERROR;
        }
        Value* fn = fr.asFunction();
        Lambda* l = caseFor(ctx, fn, _arg);

        if (expanded) return recursiveMacro ? inst = FoldResult() 
            : inst = expanded->fold(*env);
        if (l) {
            return inst = l->body()->fold(*l->scope());
        }
        return inst = FoldResult();
    }
    
    Location* Call::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        if (expanded) return expanded->gen(*env, gen, frame);
        else {
            Location* fn = _func->fold(ctx).asFunction()->gen(ctx, gen, frame);
            return frame.add(new CallInsn(_arg->gen(ctx, gen, frame), 
                fn))->value(gen, frame);
        }
    }

    // BinaryOp

    const ValueClass BinaryOp::CLASS(Builtin::CLASS);

    BinaryOp::BinaryOp(const char* opname, u32 line, u32 column, 
                       const ValueClass* vc):
        Builtin(line, column, vc), _opname(opname), lhs(nullptr), rhs(nullptr) {
        //
    }

    BinaryOp::~BinaryOp() {
        if (lhs) delete lhs;
        if (rhs) delete rhs;
    }

    const Value* BinaryOp::left() const {
        return lhs;
    }

    const Value* BinaryOp::right() const {
        return rhs;
    }

    Value* BinaryOp::left() {
        return lhs;
    }

    Value* BinaryOp::right() {
        return rhs;
    }

    void BinaryOp::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, _opname);
        if (lhs) lhs->format(io, level + 1);
        if (rhs) rhs->format(io, level + 1);
    }

    Value* BinaryOp::apply(Stack& ctx, Value* arg) {
        if (!lhs) lhs = arg;
        else if (!rhs) rhs = arg;
        return this;
    }
    
    bool BinaryOp::canApply() const {
        return !lhs || !rhs;
    }

    // UnaryOp

    const ValueClass UnaryOp::CLASS(Builtin::CLASS);

    UnaryOp::UnaryOp(const char* opname, u32 line, u32 column, 
                const ValueClass* vc):
        Builtin(line, column, vc), _opname(opname), _operand(nullptr) {
        //
    }

    UnaryOp::~UnaryOp() {
        if (_operand) delete _operand;
    }

    const Value* UnaryOp::operand() const {
        return _operand;
    }

    Value* UnaryOp::operand() {
        return _operand;
    }

    Value* UnaryOp::apply(Stack& ctx, Value* arg) {
        if (!_operand) {
            _operand = arg;
        }
        return this;
    }

    void UnaryOp::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, _opname);
        if (_operand) _operand->format(io, level + 1);
    }

    bool UnaryOp::canApply() const {
        return !_operand;
    }

    // BinaryMath

    const ValueClass BinaryMath::CLASS(BinaryOp::CLASS);

    const Type *BinaryMath::_BASE_TYPE, 
               *BinaryMath::_PARTIAL_INT, 
               *BinaryMath::_PARTIAL_UINT, 
               *BinaryMath::_PARTIAL_DOUBLE;

    const Type* BinaryMath::BASE_TYPE() {
        if (!_BASE_TYPE) _BASE_TYPE = find<IntersectionType>(set<const Type*>({
            find<FunctionType>(I64, 
                find<IntersectionType>(set<const Type*>({
                    find<FunctionType>(I64, I64),
                    find<FunctionType>(U64, U64),
                    find<FunctionType>(DOUBLE, DOUBLE)
                }))
            ),
            find<FunctionType>(U64, 
                find<IntersectionType>(set<const Type*>({
                    find<FunctionType>(I64, I64),
                    find<FunctionType>(U64, U64),
                    find<FunctionType>(DOUBLE, DOUBLE)
                }))
            ),
            find<FunctionType>(DOUBLE, find<FunctionType>(DOUBLE, DOUBLE))
        }));
        return _BASE_TYPE;
    }

    const Type* BinaryMath::PARTIAL_INT_TYPE() { 
        if (!_PARTIAL_INT) {
            _PARTIAL_INT = find<IntersectionType>(set<const Type*>({
                find<FunctionType>(I64, I64),
                find<FunctionType>(U64, U64),
                find<FunctionType>(DOUBLE, DOUBLE)
            }));
        }
        return _PARTIAL_INT;
    }

    const Type* BinaryMath::PARTIAL_UINT_TYPE() { 
        if (!_PARTIAL_UINT) {
            _PARTIAL_UINT = find<IntersectionType>(set<const Type*>({
                find<FunctionType>(I64, I64),
                find<FunctionType>(U64, U64),
                find<FunctionType>(DOUBLE, DOUBLE)
            }));
        }
        return _PARTIAL_UINT;
    }

    const Type* BinaryMath::PARTIAL_DOUBLE_TYPE() {
        if (!_PARTIAL_DOUBLE) {
            _PARTIAL_DOUBLE = find<FunctionType>(DOUBLE, DOUBLE);
        }
        return _PARTIAL_DOUBLE;
    }

    BinaryMath::BinaryMath(const char* opname, u32 line, u32 column,
                           const ValueClass* vc):
        BinaryOp(opname, line, column, vc) {
        setType(BASE_TYPE());
    }

    Value* BinaryMath::apply(Stack& ctx, Value* arg) {
        if (!lhs) {
            lhs = arg;
            if (lhs->type(ctx) == I64) setType(PARTIAL_INT_TYPE());
            else if (lhs->type(ctx) == U64) setType(PARTIAL_UINT_TYPE());
            else if (lhs->type(ctx) == DOUBLE) setType(PARTIAL_DOUBLE_TYPE());
        }
        else if (!rhs) {
            rhs = arg;

            // join types if necessary
            if (rhs->type(ctx) != lhs->type(ctx)) {
                const Type* j = join(rhs->type(ctx), lhs->type(ctx));
                if (rhs->type(ctx) != j) rhs = new Cast(j, rhs);
                else if (lhs->type(ctx) != j) lhs = new Cast(j, lhs);
            }
            setType(lhs->type(ctx));
        }
        return this;
    }

    // Add

    const ValueClass Add::CLASS(BinaryMath::CLASS);

    Add::Add(u32 line, u32 column, const ValueClass* vc):
        BinaryMath("Add", line, column, vc) {
        //
    }

    FoldResult Add::fold(Stack& ctx) {
        FoldResult l = lhs ? lhs->fold(ctx) : FoldResult();
        FoldResult r = rhs ? rhs->fold(ctx) : FoldResult();
        if (!l.isNumeric() || !r.isNumeric()) return FoldResult();
        if (l.isInt() && r.isInt()) return FoldResult(l.asInt() + r.asInt());
        double ld = l.isInt() ? l.asInt() : l.asFloat();
        double rd = r.isInt() ? r.asInt() : r.asFloat();
        return FoldResult(ld + rd);
    }

    Location* Add::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        return frame.add(
            new AddInsn(lhs->gen(ctx, gen, frame), 
                rhs->gen(ctx, gen, frame))
        )->value(gen, frame);
    }

    // Subtract

    const ValueClass Subtract::CLASS(BinaryMath::CLASS);

    Subtract::Subtract(u32 line, u32 column, const ValueClass* vc):
        BinaryMath("Subtract", line, column, vc) {
        //
    }

    FoldResult Subtract::fold(Stack& ctx) {
        FoldResult l = lhs ? lhs->fold(ctx) : FoldResult();
        FoldResult r = rhs ? rhs->fold(ctx) : FoldResult();
        if (!l.isNumeric() || !r.isNumeric()) return FoldResult();
        if (l.isInt() && r.isInt()) return FoldResult(l.asInt() - r.asInt());
        double ld = l.isInt() ? l.asInt() : l.asFloat();
        double rd = r.isInt() ? r.asInt() : r.asFloat();
        return FoldResult(ld - rd);
    }

    Location* Subtract::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        return frame.add(
            new SubInsn(lhs->gen(ctx, gen, frame), 
                rhs->gen(ctx, gen, frame))
        )->value(gen, frame);
    }

    // Multiply

    const ValueClass Multiply::CLASS(BinaryMath::CLASS);

    Multiply::Multiply(u32 line, u32 column, const ValueClass* vc):
        BinaryMath("Multiply", line, column, vc) {
        //
    }

    FoldResult Multiply::fold(Stack& ctx) {
        FoldResult l = lhs ? lhs->fold(ctx) : FoldResult();
        FoldResult r = rhs ? rhs->fold(ctx) : FoldResult();
        if (!l.isNumeric() || !r.isNumeric()) return FoldResult();
        if (l.isInt() && r.isInt()) return FoldResult(l.asInt() * r.asInt());
        double ld = l.isInt() ? l.asInt() : l.asFloat();
        double rd = r.isInt() ? r.asInt() : r.asFloat();
        return FoldResult(ld * rd);
    }

    Location* Multiply::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        return frame.add(
            new MulInsn(lhs->gen(ctx, gen, frame), 
                rhs->gen(ctx, gen, frame))
        )->value(gen, frame);
    }

    // Divide

    const ValueClass Divide::CLASS(BinaryMath::CLASS);

    Divide::Divide(u32 line, u32 column, const ValueClass* vc):
        BinaryMath("Divide", line, column, vc) {
        //
    }

    FoldResult Divide::fold(Stack& ctx) {
        FoldResult l = lhs ? lhs->fold(ctx) : FoldResult();
        FoldResult r = rhs ? rhs->fold(ctx) : FoldResult();
        if (!l.isNumeric() || !r.isNumeric()) return FoldResult();
        if (l.isInt() && r.isInt()) return FoldResult(l.asInt() / r.asInt());
        double ld = l.isInt() ? l.asInt() : l.asFloat();
        double rd = r.isInt() ? r.asInt() : r.asFloat();
        return FoldResult(ld / rd);
    }

    Location* Divide::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        return frame.add(
            new DivInsn(lhs->gen(ctx, gen, frame), 
                rhs->gen(ctx, gen, frame))
        )->value(gen, frame);
    }

    // Modulus

    const ValueClass Modulus::CLASS(BinaryMath::CLASS);

    Modulus::Modulus(u32 line, u32 column, const ValueClass* vc):
        BinaryMath("Modulus", line, column, vc) {
        //
    }

    FoldResult Modulus::fold(Stack& ctx) {
        FoldResult l = lhs ? lhs->fold(ctx) : FoldResult();
        FoldResult r = rhs ? rhs->fold(ctx) : FoldResult();
        if (!l.isNumeric() || !r.isNumeric()) return FoldResult();
        if (l.isInt() && r.isInt()) return FoldResult(l.asInt() % r.asInt());
        double ld = l.isInt() ? l.asInt() : l.asFloat();
        double rd = r.isInt() ? r.asInt() : r.asFloat();
        return FoldResult(ld - rd * i64(ld / rd));
    }

    Location* Modulus::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        return frame.add(
            new ModInsn(lhs->gen(ctx, gen, frame), 
                rhs->gen(ctx, gen, frame))
        )->value(gen, frame);
    }

    // BinaryLogic

    const Type *BinaryLogic::_BASE_TYPE, 
               *BinaryLogic::_PARTIAL_BOOL;

    const Type *BinaryLogic::BASE_TYPE() {
        if (_BASE_TYPE) return _BASE_TYPE;
        return _BASE_TYPE = find<FunctionType>(BOOL, 
            find<FunctionType>(BOOL, BOOL)
        );
    }

    const Type *BinaryLogic::PARTIAL_BOOL_TYPE() {
        if (_PARTIAL_BOOL) return _PARTIAL_BOOL;
        return _PARTIAL_BOOL = find<FunctionType>(BOOL, BOOL);
    }

    const ValueClass BinaryLogic::CLASS(BinaryOp::CLASS);

    BinaryLogic::BinaryLogic(const char* opname, u32 line, u32 column,
                const ValueClass* vc):
        BinaryOp(opname, line, column, vc) {
        setType(_BASE_TYPE);
    }

    Value* BinaryLogic::apply(Stack& ctx, Value* arg) {
        if (!lhs) {
            lhs = arg;
            setType(PARTIAL_BOOL_TYPE());
        }
        else if (!rhs) {
            rhs = arg;
            setType(BOOL);
        }
        return this;
    }

    // And

    const ValueClass And::CLASS(BinaryLogic::CLASS);

    And::And(u32 line, u32 column, const ValueClass* vc):
        BinaryLogic("And", line, column, vc) {
        //
    }

    FoldResult And::fold(Stack& ctx) {
        FoldResult l = lhs ? lhs->fold(ctx) : FoldResult();
        FoldResult r = rhs ? rhs->fold(ctx) : FoldResult();
        if (!l.isBool() || !r.isBool()) return FoldResult();
        return FoldResult(l.asBool() && r.asBool());
    }

    // Or

    const ValueClass Or::CLASS(BinaryLogic::CLASS);

    Or::Or(u32 line, u32 column, const ValueClass* vc):
        BinaryLogic("Or", line, column, vc) {
        //
    }

    FoldResult Or::fold(Stack& ctx) {
        FoldResult l = lhs ? lhs->fold(ctx) : FoldResult();
        FoldResult r = rhs ? rhs->fold(ctx) : FoldResult();
        if (!l.isBool() || !r.isBool()) return FoldResult();
        return FoldResult(l.asBool() || r.asBool());
    }

    // Xor

    const ValueClass Xor::CLASS(BinaryLogic::CLASS);

    Xor::Xor(u32 line, u32 column, const ValueClass* vc):
        BinaryLogic("Xor", line, column, vc) {
        //
    }

    FoldResult Xor::fold(Stack& ctx) {
        FoldResult l = lhs ? lhs->fold(ctx) : FoldResult();
        FoldResult r = rhs ? rhs->fold(ctx) : FoldResult();
        if (!l.isBool() || !r.isBool()) return FoldResult();
        return FoldResult(l.asBool() ^ r.asBool() ? true : false);
    }

    // Not
        
    const ValueClass Not::CLASS(UnaryOp::CLASS);

    Not::Not(u32 line, u32 column, const ValueClass* vc):
        UnaryOp("Not", line, column, vc) {
        setType(find<FunctionType>(BOOL, BOOL));
    }

    Value* Not::apply(Stack& ctx, Value* arg) {
        if (!_operand) {
            _operand = arg;
            setType(BOOL);
        }
        return this;
    }

    FoldResult Not::fold(Stack& ctx) {
        FoldResult o = _operand ? _operand->fold(ctx) : FoldResult();
        if (!o.isBool()) return FoldResult();
        return FoldResult(!o.asBool());
    }

    // BinaryEquality

    const Type *BinaryEquality::_BASE_TYPE, 
               *BinaryEquality::_PARTIAL_INT, 
               *BinaryEquality::_PARTIAL_UINT, 
               *BinaryEquality::_PARTIAL_DOUBLE, 
               *BinaryEquality::_PARTIAL_BOOL;

    const Type *BinaryEquality::BASE_TYPE() {
        if (_BASE_TYPE) return _BASE_TYPE;
        return _BASE_TYPE = find<IntersectionType>(set<const Type*>({
            find<FunctionType>(I64, 
                find<IntersectionType>(set<const Type*>({
                    find<FunctionType>(I64, BOOL),
                    find<FunctionType>(U64, BOOL),
                    find<FunctionType>(DOUBLE, BOOL)
                }))
            ),
            find<FunctionType>(U64, 
                find<IntersectionType>(set<const Type*>({
                    find<FunctionType>(I64, BOOL),
                    find<FunctionType>(U64, BOOL),
                    find<FunctionType>(DOUBLE, BOOL)
                }))
            ),
            find<FunctionType>(BOOL, find<FunctionType>(BOOL, BOOL)),
            find<FunctionType>(DOUBLE, find<FunctionType>(DOUBLE, BOOL))
        }));
    }

    const Type *BinaryEquality::PARTIAL_INT_TYPE() {
        if (_PARTIAL_INT) return _PARTIAL_INT;
        return _PARTIAL_INT = find<IntersectionType>(set<const Type*>({
            find<FunctionType>(I64, BOOL),
            find<FunctionType>(U64, BOOL),
            find<FunctionType>(DOUBLE, BOOL)
        }));
    }

    const Type *BinaryEquality::PARTIAL_UINT_TYPE() {
        if (_PARTIAL_UINT) return _PARTIAL_UINT;
        return _PARTIAL_UINT = find<IntersectionType>(set<const Type*>({
            find<FunctionType>(I64, BOOL),
            find<FunctionType>(U64, BOOL),
            find<FunctionType>(DOUBLE, BOOL)
        }));
    }

    const Type *BinaryEquality::PARTIAL_DOUBLE_TYPE() {
        if (_PARTIAL_DOUBLE) return _PARTIAL_DOUBLE;
        return _PARTIAL_DOUBLE = find<FunctionType>(DOUBLE, BOOL);
    }

    const Type *BinaryEquality::PARTIAL_BOOL_TYPE() {
        if (_PARTIAL_BOOL) return _PARTIAL_BOOL;
        return _PARTIAL_BOOL = find<FunctionType>(BOOL, BOOL);
    }    

    const ValueClass BinaryEquality::CLASS(BinaryOp::CLASS);

    BinaryEquality::BinaryEquality(const char* opname, u32 line, u32 column,
                    const ValueClass* vc):
        BinaryOp(opname, line, column, vc) {
        setType(BASE_TYPE());
    }

    Value* BinaryEquality::apply(Stack& ctx, Value* arg) {
        if (!lhs) {
            lhs = arg;
            if (lhs->type(ctx) == I64) setType(PARTIAL_INT_TYPE());
            else if (lhs->type(ctx) == U64) setType(PARTIAL_UINT_TYPE());
            else if (lhs->type(ctx) == BOOL) setType(PARTIAL_BOOL_TYPE());
            else if (lhs->type(ctx) == DOUBLE) setType(PARTIAL_DOUBLE_TYPE());
        }
        else if (!rhs) {
            rhs = arg;
            setType(BOOL);
        }
        return this;
    }

    // Equal

    const ValueClass Equal::CLASS(BinaryEquality::CLASS);

    Equal::Equal(u32 line, u32 column, const ValueClass* vc):
        BinaryEquality("Equal", line, column, vc) {
        //
    }

    FoldResult Equal::fold(Stack& ctx) {
        FoldResult l = lhs ? lhs->fold(ctx) : FoldResult();
        FoldResult r = rhs ? rhs->fold(ctx) : FoldResult();
        if (l.isBool() && r.isBool()) 
            return FoldResult(l.asBool() == r.asBool());
        if (l.isInt() && r.isInt()) 
            return FoldResult(l.asInt() == r.asInt());
        if (l.isFloat() && r.isFloat())
            return FoldResult(l.asFloat() == r.asFloat());
        return FoldResult();
    }

    // Inequal

    const ValueClass Inequal::CLASS(BinaryEquality::CLASS);

    Inequal::Inequal(u32 line, u32 column, const ValueClass* vc):
        BinaryEquality("Inequal", line, column, vc) {
        //
    }

    FoldResult Inequal::fold(Stack& ctx) {
        FoldResult l = lhs ? lhs->fold(ctx) : FoldResult();
        FoldResult r = rhs ? rhs->fold(ctx) : FoldResult();
        if (l.isBool() && r.isBool()) 
            return FoldResult(l.asBool() != r.asBool());
        if (l.isInt() && r.isInt()) 
            return FoldResult(l.asInt() != r.asInt());
        if (l.isFloat() && r.isFloat())
            return FoldResult(l.asFloat() != r.asFloat());
        return FoldResult();
    }

    // BinaryRelation

    const Type *BinaryRelation::_BASE_TYPE, 
               *BinaryRelation::_PARTIAL_INT, 
               *BinaryRelation::_PARTIAL_UINT, 
               *BinaryRelation::_PARTIAL_DOUBLE;

    const Type *BinaryRelation::BASE_TYPE() {
        if (_BASE_TYPE) return _BASE_TYPE;
        return _BASE_TYPE = find<IntersectionType>(set<const Type*>({
            find<FunctionType>(I64, 
                find<IntersectionType>(set<const Type*>({
                    find<FunctionType>(I64, BOOL),
                    find<FunctionType>(U64, BOOL),
                    find<FunctionType>(DOUBLE, BOOL)
                }))
            ),
            find<FunctionType>(U64, 
                find<IntersectionType>(set<const Type*>({
                    find<FunctionType>(I64, BOOL),
                    find<FunctionType>(U64, BOOL),
                    find<FunctionType>(DOUBLE, BOOL)
                }))
            ),
            find<FunctionType>(DOUBLE, find<FunctionType>(DOUBLE, BOOL))
        }));
    }

    const Type *BinaryRelation::PARTIAL_INT_TYPE() {
        if (_PARTIAL_INT) return _PARTIAL_INT;
        return _PARTIAL_INT = find<IntersectionType>(set<const Type*>({
            find<FunctionType>(I64, BOOL),
            find<FunctionType>(U64, BOOL),
            find<FunctionType>(DOUBLE, BOOL)
        }));
    }

    const Type *BinaryRelation::PARTIAL_UINT_TYPE() {
        if (_PARTIAL_UINT) return _PARTIAL_UINT;
        return _PARTIAL_UINT = find<IntersectionType>(set<const Type*>({
            find<FunctionType>(I64, BOOL),
            find<FunctionType>(U64, BOOL),
            find<FunctionType>(DOUBLE, BOOL)
        }));
    }

    const Type *BinaryRelation::PARTIAL_DOUBLE_TYPE() {
        if (_PARTIAL_DOUBLE) return _PARTIAL_DOUBLE;
        return _PARTIAL_DOUBLE = find<FunctionType>(DOUBLE, BOOL);
    } 

    const ValueClass BinaryRelation::CLASS(BinaryOp::CLASS);

    BinaryRelation::BinaryRelation(const char* opname, u32 line, u32 column,
                    const ValueClass* vc):
        BinaryOp(opname, line, column, vc) {
        setType(BASE_TYPE());
    }

    Value* BinaryRelation::apply(Stack& ctx, Value* arg) {
        if (!lhs) {
            lhs = arg;
            if (lhs->type(ctx) == I64) setType(PARTIAL_INT_TYPE());
            else if (lhs->type(ctx) == U64) setType(PARTIAL_UINT_TYPE());
            else if (lhs->type(ctx) == DOUBLE) setType(PARTIAL_DOUBLE_TYPE());
        }
        else if (!rhs) {
            rhs = arg;
            setType(BOOL);
        }
        return this;
    }

    // Less

    const ValueClass Less::CLASS(BinaryRelation::CLASS);

    Less::Less(u32 line, u32 column, const ValueClass* vc):
        BinaryRelation("Less", line, column, vc) {
        //
    }

    FoldResult Less::fold(Stack& ctx) {
        FoldResult l = lhs ? lhs->fold(ctx) : FoldResult();
        FoldResult r = rhs ? rhs->fold(ctx) : FoldResult();
        if (l.isInt() && r.isInt()) 
            return FoldResult(l.asInt() < r.asInt());
        if (l.isFloat() && r.isFloat())
            return FoldResult(l.asFloat() < r.asFloat());
        return FoldResult();
    }

    // LessEqual

    const ValueClass LessEqual::CLASS(BinaryRelation::CLASS);

    LessEqual::LessEqual(u32 line, u32 column, const ValueClass* vc):
        BinaryRelation("LessEqual", line, column, vc) {
        //
    }

    FoldResult LessEqual::fold(Stack& ctx) {
        FoldResult l = lhs ? lhs->fold(ctx) : FoldResult();
        FoldResult r = rhs ? rhs->fold(ctx) : FoldResult();
        if (l.isInt() && r.isInt()) 
            return FoldResult(l.asInt() <= r.asInt());
        if (l.isFloat() && r.isFloat())
            return FoldResult(l.asFloat() <= r.asFloat());
        return FoldResult();
    }

    // Greater

    const ValueClass Greater::CLASS(BinaryRelation::CLASS);

    Greater::Greater(u32 line, u32 column, const ValueClass* vc):
        BinaryRelation("Greater", line, column, vc) {
        //
    }

    FoldResult Greater::fold(Stack& ctx) {
        FoldResult l = lhs ? lhs->fold(ctx) : FoldResult();
        FoldResult r = rhs ? rhs->fold(ctx) : FoldResult();
        if (l.isInt() && r.isInt()) 
            return FoldResult(l.asInt() > r.asInt());
        if (l.isFloat() && r.isFloat())
            return FoldResult(l.asFloat() > r.asFloat());
        return FoldResult();
    }

    // GreaterEqual

    const ValueClass GreaterEqual::CLASS(BinaryRelation::CLASS);

    GreaterEqual::GreaterEqual(u32 line, u32 column, const ValueClass* vc):
        BinaryRelation("GreaterEqual", line, column, vc) {
        //
    }

    FoldResult GreaterEqual::fold(Stack& ctx) {
        FoldResult l = lhs ? lhs->fold(ctx) : FoldResult();
        FoldResult r = rhs ? rhs->fold(ctx) : FoldResult();
        if (l.isInt() && r.isInt()) 
            return FoldResult(l.asInt() >= r.asInt());
        if (l.isFloat() && r.isFloat())
            return FoldResult(l.asFloat() >= r.asFloat());
        return FoldResult();
    }

    // Join

    const Type* Join::_BASE_TYPE;

    const Type* Join::BASE_TYPE() {
        if (_BASE_TYPE) return _BASE_TYPE;
        return _BASE_TYPE = find<IntersectionType>(set<const Type*>({
            find<FunctionType>(TYPE, find<FunctionType>(TYPE, TYPE)),
            find<FunctionType>(ANY, find<FunctionType>(ANY, ANY))
        }));
    }

    const ValueClass Join::CLASS(BinaryOp::CLASS);

    Join::Join(u32 line, u32 column, const ValueClass* vc):
        BinaryOp("Join", line, column, vc) {
        setType(BASE_TYPE());
    }

    Value* Join::apply(Stack& ctx, Value* arg) {
        if (!lhs) {
            lhs = arg;
            if (lhs->type(ctx) == TYPE) setType(find<FunctionType>(TYPE, TYPE));
            else setType(find<FunctionType>(ANY, ANY));
        }
        else if (!rhs) {
            rhs = arg;

            setType(find<TupleType>(vector<const Type*>({
                lhs->type(ctx), rhs->type(ctx)
            })));
        }
        return this;
    }

    FoldResult Join::fold(Stack& ctx) {
        if (type(ctx)->explicitly(TYPE)) {
            FoldResult lf = lhs->fold(ctx), rf = rhs->fold(ctx);
            if (!lf.isType() || !rf.isType()) {
                err(PHASE_TYPE, line(), column(),
                    "Incomplete type in tuple.");
                return FoldResult();
            }
            return find<TupleType>(vector<const Type*>({
                lf.asType(), 
                rf.asType()
            }));
        }
        return FoldResult();
    }
    
    bool Join::lvalue(Stack& ctx) const {
        return lhs->lvalue(ctx) && rhs->lvalue(ctx);
    }

    // Intersect

    const Type* Intersect::_BASE_TYPE;

    const Type* Intersect::BASE_TYPE() {
        if (_BASE_TYPE) return _BASE_TYPE;
        return _BASE_TYPE = find<IntersectionType>(set<const Type*>({
            find<FunctionType>(TYPE, find<FunctionType>(TYPE, TYPE)),
            find<FunctionType>(ANY, find<FunctionType>(ANY, ANY))
        }));
    }

    const ValueClass Intersect::CLASS(BinaryOp::CLASS);

    const Type* Intersect::lazyType(Stack& ctx) {
        if (!lhs && !rhs) return _BASE_TYPE;
        else if (!rhs) return find<FunctionType>(
            ANY, find<FunctionType>(ANY, ANY)
        );

        const Type* lt = lhs->type(ctx);
        const Type* rt = rhs->type(ctx);
        
        if (lt->is<FunctionType>() && rt->is<FunctionType>()) {
            const FunctionType* lf = lt->as<FunctionType>();
            const FunctionType* rf = rt->as<FunctionType>();
            if (lf->arg() == rf->arg() 
                && (lf->ret()->explicitly(rf->ret())
                    || rf->ret()->explicitly(lf->ret()) 
                    || lf->ret() == ANY 
                    || rf->ret() == ANY)
                && !lf->conflictsWith(rf) && !rf->conflictsWith(lf)) {
                vector<Constraint> constraints;
                for (auto& c : lf->constraints()) constraints.push(c);
                for (auto& c : rf->constraints()) constraints.push(c);
                const Type* ret = join(lf->ret(), rf->ret());
                if (ret == ANY) 
                    ret = lf->ret() == ANY ? rf->ret() : lf->ret();
                return find<FunctionType>(lf->arg(), ret, constraints);
            }
            else {
                return find<IntersectionType>(set<const Type*>({ 
                    lt, rt 
                }));
            }
        }
        else {
            return find<IntersectionType>(set<const Type*>({ lt, rt }));
        }
    }

    void Intersect::populate(Stack& ctx, map<const Type*, 
                             vector<Value*>>& values) {
        if (lhs) {
            if (lhs->is<Intersect>()) {
                lhs->as<Intersect>()->populate(ctx, values);
            }
            else if (lhs->type(ctx)->is<FunctionType>()) {
                const FunctionType* ft = lhs->type(ctx)->as<FunctionType>();
                values[ft->arg()].push(lhs);
            }
            else values[nullptr].push(lhs);
            lhs = nullptr;
        }
        if (rhs) {
            if (rhs->is<Intersect>()) {
                rhs->as<Intersect>()->populate(ctx, values);
            }
            else if (rhs->type(ctx)->is<FunctionType>()) {
                const FunctionType* ft = rhs->type(ctx)->as<FunctionType>();
                values[ft->arg()].push(rhs);
            }
            else values[nullptr].push(rhs);
            rhs = nullptr;
        }
        delete this;
    }
    
    void Intersect::getFunctions(Stack& ctx, vector<Lambda*>& functions) {
        if (lhs) {
            if (lhs->type(ctx)->is<FunctionType>()) {
                FoldResult fr = lhs->fold(ctx);
                if (fr.isFunction() && fr.asFunction()->is<Lambda>())
                    functions.push(fr.asFunction()->as<Lambda>());
            }
            if (lhs->is<Intersect>()) 
                lhs->as<Intersect>()->getFunctions(ctx, functions);
        }
        if (rhs) {
            if (lhs->type(ctx)->is<FunctionType>()) {
                FoldResult fr = rhs->fold(ctx);
                if (fr.isFunction() && fr.asFunction()->is<Lambda>())
                    functions.push(fr.asFunction()->as<Lambda>());
            }
            if (rhs->is<Intersect>()) 
                rhs->as<Intersect>()->getFunctions(ctx, functions);
        }
    }

    Intersect::Intersect(u32 line, u32 column, const ValueClass* vc):
        BinaryOp("Intersect", line, column, vc) {
        setType(BASE_TYPE());
    }

    FoldResult Intersect::fold(Stack& ctx) {
        if (type(ctx)->explicitly(TYPE)) {
            return find<IntersectionType>(set<const Type*>({
                lhs->fold(ctx).asType(), 
                rhs->fold(ctx).asType()
            }));
        }
        if (type(ctx)->is<FunctionType>()) return FoldResult(this);
        return FoldResult();
    }
    
    Value* Intersect::apply(Stack& ctx, Value* arg) {
        if (!lhs) {
            lhs = arg;
            if (lhs->type(ctx) == TYPE) setType(find<FunctionType>(TYPE, TYPE));
            else setType(find<FunctionType>(ANY, ANY));
        }
        else if (!rhs) {
            rhs = arg;
            map<const Type*, vector<Value*>> values;
            const Type* lt = lhs->type(ctx);
            const Type* rt = rhs->type(ctx);

            if (lhs->is<Intersect>()) {
                lhs->as<Intersect>()->populate(ctx, values);
            }
            else if (lt->is<FunctionType>()) {
                const FunctionType* ft = lt->as<FunctionType>();
                values[ft->arg()].push(lhs);
            }
            else values[nullptr].push(lhs);

            if (rhs->is<Intersect>()) {
                rhs->as<Intersect>()->populate(ctx, values);
            }
            else if (rt->is<FunctionType>()) {
                const FunctionType* ft = rt->as<FunctionType>();
                values[ft->arg()].push(rhs);
            }
            else values[nullptr].push(rhs);

            map<const FunctionType*, Value*> folded;
            for (auto& entry : values) {
                const FunctionType* etype = nullptr;
                if (!entry.second.size()) continue;
                Value* v = entry.second[0];
                if (v->type(ctx)->is<FunctionType>())
                    etype = v->type(ctx)->as<FunctionType>();
                for (u32 i = 1; i < entry.second.size(); i ++) {
                    if (entry.second[i]->type(ctx)->
                        conflictsWith(v->type(ctx))) {
                        buffer b;
                        fprint(b, "Cannot create intersection; types '");
                        v->type(ctx)->format(b);
                        fprint(b, "' and '");
                        entry.second[i]->type(ctx)->format(b);
                        fprint(b, "' overlap.");
                        err(PHASE_TYPE, entry.second[i]->line(), 
                            entry.second[i]->column(), b);
                        continue;
                    }
                    if (etype && 
                        entry.second[i]->type(ctx)->is<FunctionType>()) {
                        const FunctionType* eft = 
                            entry.second[i]->type(ctx)->as<FunctionType>();
                        if (etype->ret() != eft->ret()
                            && eft->ret() != ANY && etype->ret() != ANY) {
                            err(PHASE_TYPE, entry.second[i]->line(),
                                entry.second[i]->column(),
                                "Cannot create intersection; types '",
                                (const Type*)etype, "' and '", 
                                (const Type*)eft, "' would result in ",
                                "ambiguous function.");
                            continue;
                        }

                        // otherwise, if types match or one is any, 
                        // update etype
                        if (etype->ret() == ANY) etype = 
                            find<FunctionType>(eft->arg(), eft->ret());
                    }

                    Intersect* in = new Intersect(v->line(), v->column());
                    in->lhs = v;
                    in->rhs = entry.second[i];
                    in->updateType(ctx);
                    v = in;
                }
                folded[etype] = v;
            }

            Value* v = nullptr;
            for (auto& entry : folded) {
                if (!v) v = entry.second;
                else {
                    Intersect* in = new Intersect(v->line(), v->column());
                    in->lhs = v;
                    in->rhs = entry.second;
                    in->updateType(ctx);
                    v = in;
                }
            }

            if (!v->is<Intersect>()) {
                lhs = nullptr;
                rhs = nullptr;
                setType(ERROR);
                return this;
            }
            else {
                lhs = v->as<Intersect>()->lhs;
                rhs = v->as<Intersect>()->rhs;
                v->as<Intersect>()->lhs = v->as<Intersect>()->rhs = nullptr;
                delete v; 
            }

            lt = lhs->type(ctx);
            rt = rhs->type(ctx);
            
            if (lt->is<FunctionType>() && rt->is<FunctionType>()) {
                const FunctionType* lf = lt->as<FunctionType>();
                const FunctionType* rf = rt->as<FunctionType>();
                if (lf->arg() == rf->arg() 
                    && (lf->ret() == rf->ret() 
                        || lf->ret() == ANY 
                        || rf->ret() == ANY)
                    && !lf->conflictsWith(rf) && !rf->conflictsWith(lf)) {
                    vector<Constraint> constraints;
                    for (auto& c : lf->constraints()) constraints.push(c);
                    for (auto& c : rf->constraints()) constraints.push(c);
                    setType(find<FunctionType>(lf->arg(), lf->ret() == ANY ? 
                        rf->ret() : lf->ret(), constraints));
                }
                else {
                    setType(find<IntersectionType>(set<const Type*>({ 
                        lt, rt 
                    })));
                }
            }
            else {
                setType(find<IntersectionType>(set<const Type*>({ lt, rt })));
            }

            // println(_stdout, "intersection type is ", type(ctx));
        }

        return this;
    }

    void Intersect::bindrec(const ustring& name, const Type* type,
                          FoldResult value) {
        if (lhs->is<Lambda>()) {
            lhs->as<Lambda>()->bindrec(name, type, value);
        }
        if (rhs->is<Lambda>()) {
            rhs->as<Lambda>()->bindrec(name, type, value);
        }
        if (lhs->is<Intersect>()) {
            lhs->as<Intersect>()->bindrec(name, type, value);
        }
        if (rhs->is<Intersect>()) {
            rhs->as<Intersect>()->bindrec(name, type, value);
        }
    }

    Constraint lambdaMatches(Lambda* fn, Stack& ctx, FoldResult value) {
        const FunctionType* ft = fn->type(ctx)->as<FunctionType>();
        if (auto match = ft->matches(value)) return match;
        return Constraint::NONE;
    }

    Lambda* Intersect::caseFor(Stack& ctx, FoldResult value) {
        auto it = _casecache.find(value);
        if (it != _casecache.end()) return it->second;

        Lambda *l = nullptr, *r = nullptr;
        Constraint left = Constraint::NONE, right = Constraint::NONE;
        if (lhs->is<Lambda>()) {
            left = lambdaMatches(lhs->as<Lambda>(), ctx, value);
            l = lhs->as<Lambda>();
        }
        if (rhs->is<Lambda>()) {
            right = lambdaMatches(rhs->as<Lambda>(), ctx, value);
            r = rhs->as<Lambda>();
        }
        if (lhs->is<Intersect>()) {
            l = lhs->as<Intersect>()->caseFor(ctx, value);
            left = maxMatch(l->type(ctx)->as<FunctionType>()
                ->constraints(), value);
        }
        if (rhs->is<Intersect>()) {
            r = rhs->as<Intersect>()->caseFor(ctx, value);
            right = maxMatch(r->type(ctx)->as<FunctionType>()
                ->constraints(), value);
        }

        if (!left && !right) return nullptr;
        else if (left) return _casecache[value] = l;
        else if (right) return _casecache[value] = r;
        else if (left.precedes(right) || 
            (left.type() != UNKNOWN && right.type() == UNKNOWN)) return _casecache[value] = l;
        else return _casecache[value] = r;
    }

    Value* Intersect::expand(Stack* body) {
        Intersect* in = new Intersect(line(), column());
        if (lhs->is<Lambda>()) in->lhs = lhs->as<Lambda>()->expand(body);
        if (lhs->is<Intersect>()) in->lhs = lhs->as<Intersect>()->expand(body);
        if (rhs->is<Lambda>()) in->rhs = rhs->as<Lambda>()->expand(body);
        if (rhs->is<Intersect>()) in->rhs = rhs->as<Intersect>()->expand(body);
        in->updateType(*body);
        return in;
    }

    Location* Intersect::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        if (type(ctx)->isMeta()) return frame.none();
        if (type(ctx)->is<FunctionType>()) {
            auto ft = type(ctx)->as<FunctionType>();
            if (!_label.size()) {
                Function* func = gen.newFunction();
                _label = func->label();
                vector<Lambda*> cases;
                getFunctions(ctx, cases);

                vector<Lambda*> constrained;
                Lambda* wildcard = nullptr;

                for (Lambda* l : cases) { 
                    auto& cons = l->type(ctx)->
                        as<FunctionType>()->constraints();
                    bool equals = false;
                    for (auto& con : cons) {
                        if (con.type() == EQUALS_VALUE) {
                            constrained.push(l);
                            equals = true;
                            break;
                        }
                    }
                    if (!equals) wildcard = l;
                }

                Location* arg = func->stack(ft->arg());
                func->add(new MovInsn(arg, gen.locateArg(ft->arg())));
                Location* retval = ft->ret() == VOID ? frame.none()
                    : func->stack(ft->ret());

                ustring end = gen.newLabel();
                vector<ustring> labels;
                vector<Location*> callables;
                for (u32 i = 0; i < constrained.size(); i ++) {
                    labels.push(gen.newLabel());
                }

                for (u32 i = 0; i < constrained.size(); i ++) {
                    // callables.push(constrained[i]->gen(ctx, gen, *func));
                    auto& cons = constrained[i]->type(ctx)->
                        as<FunctionType>()->constraints();
                    func->add(new IfEqualInsn(
                        cons[0].value().gen(gen, *func),
                        arg,
                        labels[i]
                    ));
                }

                Location* call = arg;
                if (wildcard) {
                    call = wildcard->genInline(ctx, arg, gen, *func);
                }
                if (*retval) func->add(new MovInsn(retval, call));
                func->add(new GotoInsn(end));

                for (u32 i = 0; i < constrained.size(); i ++) {
                    func->add(new Label(labels[i], false));

                    Location* call = constrained[i]->genInline(ctx, arg, gen, *func);
                    if (*retval) func->add(new MovInsn(retval, call));
                    func->add(new GotoInsn(end));
                }

                func->add(new Label(end, false));
                if (*retval) func->add(new RetInsn(retval));
            }
            Location* loc = frame.stack(type(ctx));
            frame.add(new LeaInsn(loc, _label));
            return loc;
        }
        return frame.none();
    }

    // Define

    const ValueClass Define::CLASS(Builtin::CLASS);

    Define::Define(Value* type, const ustring& name,
                   const ValueClass* vc):
        Builtin(type->line(), type->column(), vc), _type(type), _name(name) {
        //
    }

    Define::~Define() {
        if (_type) delete _type;
    }

    void Define::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Define ", _name);
        _type->format(io, level + 1);
    }

    Value* Define::apply(Stack& ctx, Value* arg) {
        FoldResult fr = _type->fold(ctx);
        if (!fr.isType()) {
            err(PHASE_TYPE, line(), column(),
                "Expected type expression, got '", fr.toString(), "'.");
            setType(ERROR);
        }
        else if (ctx.nearestScope().find(_name) != ctx.nearestScope().end()) {
            err(PHASE_TYPE, line(), column(),
                "Redefinition of variable '", _name, "'.");
            setType(ERROR);
        }
        else {
            setType(fr.asType());
            ctx.bind(_name, type(ctx));
        }

        return this;
    }

    bool Define::lvalue(Stack& ctx) const {
        return true;
    }

    Stack::Entry* Define::entry(Stack& ctx) const {
        return ctx[_name];
    }
    
    bool Define::canApply() const {
        return !_name.size() || !_type;
    }

    Location* Define::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        if (type(ctx)->isMeta()) return frame.none();

        entry(ctx)->loc() = frame.stack(type(ctx), _name);
        return entry(ctx)->loc();
    }

    // Autodefine

    const ValueClass Autodefine::CLASS(Builtin::CLASS);

    Autodefine::Autodefine(u32 line, u32 column,
                           const ValueClass* vc):
        Builtin(line, column, vc), _name(nullptr), _init(nullptr) {
        setType(find<FunctionType>(META, find<FunctionType>(ANY, VOID)));
    }

    Autodefine::~Autodefine() {
        if (_name) delete _name;
        if (_init) delete _init;
    }

    void Autodefine::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Define");
        if (_name) _name->format(io, level + 1);
        if (_init) _init->format(io, level + 1);
    }

    void bind(Stack& ctx, Value* dst, Value* src) {
        if (dst->is<Variable>()) {
            const ustring& name = dst->as<Variable>()->name();
            if (ctx.nearestScope().find(name) != ctx.nearestScope().end()) {
                err(PHASE_TYPE, dst->line(), dst->column(),
                    "Redefinition of variable '", name, "'.");
                return;
            }
            ctx.bind(name, src->type(ctx));
            if (auto val = src->entry(ctx)) {
                if (val->builtin()) ctx[name]->builtin() = val->builtin();
                if (val->value()) ctx[name]->value() = val->value();
            }
            else if (src->fold(ctx)) {
                ctx[name]->value() = src->fold(ctx);
            }
            if (src->is<Lambda>()) {
                src->as<Lambda>()->bindrec(name, src->type(ctx),
                    src->fold(ctx));
            }
            if (src->is<Intersect>()) {
                src->as<Intersect>()->bindrec(name, src->type(ctx),
                    src->fold(ctx));
            }
        }
        else if (dst->is<Join>()) {
            if (!src->is<Join>()) {
                err(PHASE_TYPE, src->line(), src->column(),
                    "Attempted to bind multiple variables to ",
                    "non-tuple value.");
            }
            bind(ctx, dst->as<Join>()->left(), src->as<Join>()->left());
            bind(ctx, dst->as<Join>()->right(), src->as<Join>()->right());
        }
    }

    Value* Autodefine::apply(Stack& ctx, Value* arg) {
        if (!_name) {
            if (!arg->is<Quote>()) {
                err(PHASE_TYPE, arg->line(), arg->column(),
                    "Expected symbol.");
                return this;
            }
            catchErrors();
            u32 prev = ctx.size();
            arg->as<Quote>()->term()->eval(ctx);
            discardErrors();
            if (ctx.size() == prev + 1 && ctx.top()->is<Variable>()) {
                _name = ctx.pop();;
            }
            else if (ctx.size() == prev + 1 && ctx.top()->is<Join>()) {
                _name = ctx.pop();
            }
            else {
                err(PHASE_TYPE, arg->line(), arg->column(),
                    "Expected symbol.");
            }
            setType(find<FunctionType>(ANY, VOID));
        }
        else if (!_init) {
            _init = arg;
            bind(ctx, _name, _init);
            setType(VOID);
        }
        return this;
    }
    
    bool Autodefine::canApply() const {
        return !_name || !_init;
    }

    bool Autodefine::lvalue(Stack& ctx) const {
        return true;
    }

    Stack::Entry* Autodefine::entry(Stack& ctx) const {
        return nullptr;
    }

    Location* Autodefine::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        if (_init->type(ctx)->isMeta()) return frame.none();

        auto e = _name->entry(ctx);
        if (!e->reassigned() && _init->type(ctx)->is<FunctionType>()
            && _name->is<Variable>()) {
            FoldResult fr = _init->fold(ctx);
            if (fr.isFunction() && fr.asFunction()->is<Lambda>()) {
                fr.asFunction()->as<Lambda>()->
                    addAltLabel(_name->as<Variable>()->name());
            }
        }
        if (_name->is<Variable>())
            e->loc() = frame.stack(_init->type(ctx), _name->as<Variable>()->name());
        else e->loc() = frame.stack(_init->type(ctx));
        frame.add(new MovInsn(e->loc(), _init->gen(ctx, gen, frame)));
        return e->loc();
    }

    // Assign

    const ValueClass Assign::CLASS(Value::CLASS);

    Assign::Assign(Value* left, Value* right, 
                   u32 line, u32 column, const ValueClass* vc):
        Value(line, column, vc), lhs(left), rhs(right) {
        setType(VOID);
    }

    Assign::~Assign() {
        if (lhs) delete lhs;
        if (rhs) delete rhs;
    }

    void assign(Stack& ctx, Value* dst, Value* src) {
        if (dst->is<Join>()) {
            if (!src->is<Join>()) {
                err(PHASE_TYPE, src->line(), src->column(),
                    "Attempted to assign multiple variables to ",
                    "non-tuple value.");
            }
            assign(ctx, dst->as<Join>()->left(), src->as<Join>()->left());
            assign(ctx, dst->as<Join>()->right(), src->as<Join>()->right());
        }
        else if (dst->is<Variable>() || dst->is<Define>()) {
            if (auto val = src->entry(ctx)) {
                auto entry = dst->entry(ctx);
                if (dst->is<Variable>()) entry->reassign();
                if (val->builtin()) entry->builtin() = val->builtin();
                if (val->value()) entry->value() = val->value();
            }
            else if (src->fold(ctx)) {
                auto entry = dst->entry(ctx);
                if (dst->is<Variable>()) entry->reassign();
                entry->value() = src->fold(ctx);
            }
        }
    }
    
    void Assign::apply(Stack& ctx) {
        if (lhs->is<Autodefine>()) {
            lhs->as<Autodefine>()->apply(ctx, rhs);
            ctx.push(lhs);
            lhs = rhs = nullptr;
            delete this;
            return;
        }
        else {
            if (!rhs->type(ctx)->explicitly(lhs->type(ctx))) {
                err(PHASE_TYPE, line(), column(),
                    "Incompatible types in assignment; attempted to bind '",
                    lhs->type(ctx), "' to '", rhs->type(ctx), "'.");
            }
            assign(ctx, lhs, rhs);
            if (rhs->type(ctx) != lhs->type(ctx)) {
                rhs = new Cast(lhs->type(ctx), rhs);
            }
        }
        ctx.push(this);
    }

    void Assign::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Assign");
        lhs->format(io, level + 1);
        rhs->format(io, level + 1);
    }

    bool Assign::lvalue(Stack& ctx) const {
        return true;
    }

    Stack::Entry* Assign::entry(Stack& ctx) const {
        return lhs->entry(ctx);
    }

    Location* Assign::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        if (lhs->type(ctx)->isMeta()) return frame.none();
        frame.add(new MovInsn(lhs->gen(ctx, gen, frame), rhs->gen(ctx, gen, frame)));
        return frame.none();
    }

    // Print

    const Type* Print::_BASE_TYPE;

    const Type* Print::BASE_TYPE() {
        if (_BASE_TYPE) return _BASE_TYPE;
        return _BASE_TYPE = find<IntersectionType>(set<const Type*>({
            find<FunctionType>(STRING, VOID),
            find<FunctionType>(CHAR, VOID),
            find<FunctionType>(BOOL, VOID),
            find<FunctionType>(I64, VOID),
            find<FunctionType>(DOUBLE, VOID)
        }));
    }

    const ValueClass Print::CLASS(UnaryOp::CLASS);

    Print::Print(u32 line, u32 column, const ValueClass* vc):
        UnaryOp("Print", line, column, vc) {
        setType(BASE_TYPE());
    }

    Value* Print::apply(Stack& ctx, Value* arg) {
        if (!_operand) {
            _operand = arg;
            setType(VOID);
        }
        return this;
    }
    
    Location* Print::gen(Stack& ctx, CodeGenerator& gen, CodeFrame& frame) {
        frame.add(new PrintInsn(_operand->gen(ctx, gen, frame)));
        return frame.none();
    }

    // Cast

    const ValueClass Cast::CLASS(Value::CLASS);

    Cast::Cast(const Type* dst, Value* src,
                               const ValueClass* vc):
        Value(src->line(), src->column(), vc), _dst(dst), _src(src) {
        setType(dst);
    }
                    
    void Cast::format(stream& io, u32 level) const {
        indent(io, level);
        _dst->format(io);
        println(io, " cast");
        _src->format(io, level + 1);
    }
    
    FoldResult Cast::fold(Stack& ctx) {
        // TODO: casting rules
        if (_dst == TYPE) {
            const Type* t = _src->type(ctx);
            if (t->is<FunctionType>()) {
                if (!_src->fold(ctx).isFunction()) {
                    err(PHASE_TYPE, line(), column(),
                        "Cannot find function.");
                    return {};
                }
                auto& cons = t->as<FunctionType>()->constraints();
                if (cons.size() != 1 || cons[0].type() != EQUALS_VALUE
                    || !cons[0].value().isType()) {
                    err(PHASE_TYPE, line(), column(),
                        "Cannot convert function to type object.");
                    return {};
                }
                else {    
                    Value* fn = _src->fold(ctx).asFunction();
                    const Type* bt = nullptr;
                    if (fn->is<Lambda>()) {
                        bt = fn->as<Lambda>()->body()->fold(ctx).asType();
                    }
                    return find<FunctionType>(cons[0].value().asType(), bt);
                }
            }
            return _src->fold(ctx);
        }
        return {};
    }

    // Eval

    const ValueClass Eval::CLASS(Builtin::CLASS);

    Eval::Eval(u32 line, u32 column, const ValueClass* vc):
        Builtin(line, column, vc) {
        setType(find<FunctionType>(META, ANY));
    }

    void Eval::format(stream& io, u32 level) const {
        indent(io, level);
        println(io, "Eval");
    }

    Value* Eval::apply(Stack& ctx, Value* v) {
        if (!v) return this;
        FoldResult fr = v->fold(ctx);
        if (!fr.isTerm()) {
            err(PHASE_TYPE, line(), column(),
                "Could not evaluate value.");
            return nullptr;
        }
        fr.asTerm()->eval(ctx);
        return nullptr;
    }
}